/*
 * COPY RIGHT INFORMATION
 * ---------------------------
 * Mr.Jeevan Mathew ,
 * Kulathapappallil House ,
 * Kadakkad ,
 * Pandalam PO 689501, Kerala ,
 * Email ID : jmathew@gmx.de 
 *         AND
 * UVJ Technologies,
 * 1st Floor Thapasya Building , 
 * Infopark , Kochi,
 * Kerala,India ,682042  
 * CIN – U72200KL2003PTC016041 
 * Export code IEC1004008490 
 * retain all ownership and intellectual property rights in the code accompanying this message and in all derivatives hereto.
 *
 * REVISION HISTORY
 * ---------------------------
 * @author   : Arjun K
 * @since    : 22/03/2018
 * @comments :
 *
 * ---------------------------
 *
 * DESCRIPTION
 * ---------------------------
 *
 *
 *
 * ---------------------------
 */
package com.jeevan.virtualcockpit.aiflux;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataAnalysis {

    private static final List<Float> variances = new ArrayList<>();
    private static float averageSpeed = 0;

    /**
     * Method to analyse speed fluctuations in a particular time interval by
     * dividing them into blocks with a window size
     *
     * @param fluctuations List speed values monitored in a time interval.
     * @return array of data consists of abnormal up and down count.
     * @since 15/05/2018
     */
    public static int[] analyseFluctuation(List fluctuations) {
        averageSpeed = getAverage(fluctuations);
        List<Integer> maxOfBlock = new ArrayList<>();
        for (int i = 0; i < fluctuations.size(); i += CarData.WINDOW_BLOCK_SIZE) {
            List<Integer> group = fluctuations.subList(i, (i + CarData.WINDOW_BLOCK_SIZE > fluctuations.size() ? fluctuations.size() : i + CarData.WINDOW_BLOCK_SIZE));
            maxOfBlock.add(getMaxOfBlock(new ArrayList<>(group)));
        }
        return getAbnormalUpDownCount(maxOfBlock);
    }

    private static float getAverage(List<Integer> block) {
        float sum = 0;
        float avg = 0;
        for (int i : block) {
            sum += i;
        }
        avg = sum / block.size();
        return avg;
    }

    private static int getMaxOfBlock(List<Integer> block) {
        return Collections.max(block);
    }

    private static void getVariance(List<Float> blockAverage) {
        for (int i = 0; i < blockAverage.size() - 1; i++) {
            variances.add(Math.abs(blockAverage.get(i + 1) - blockAverage.get(i)));
        }
    }

    public static int[] getAbnormalUpDownCount(List<Integer> maxOfBlock) {
        int diff = 0, UP_COUNT = 0, DOWN_COUNT = 0;
        int valueArray[] = new int[2];
        for (int i = 0; i < maxOfBlock.size() - 1; i++) {
            diff = maxOfBlock.get(i + 1) - maxOfBlock.get(i);
            if (diff > CarData.DIFF_LIMIT) {
                UP_COUNT++;
                CarData.DISTANCE_TRAVELLED = calculateDistance();
            } else if (diff < -(CarData.DIFF_LIMIT)) {
                DOWN_COUNT++;
                CarData.DISTANCE_TRAVELLED = calculateDistance();
            }
        }
        valueArray[0] = UP_COUNT;
        valueArray[1] = DOWN_COUNT;
        return valueArray;
    }

    //Testing
    public static int calculateDriveScore(List driveSpeed, List driveRpm, long delay) {
        float driveScore = 0;
        float avgSpeed = getAverage(driveSpeed);
        float avgRPM = getAverage(driveRpm);
        driveScore = (CarData.speedWeightage * (avgSpeed / CarData.optimalSpeed))
                + (CarData.rpmWeightage * (avgRPM / CarData.optimalRPM));
        if (driveScore > 100) {
            driveScore = 100 - (driveScore - 100);
        }
        if(driveScore < 0)
            driveScore = 0;
        return (int) driveScore;
    }

    private static double convertTime(long milliseconds) {
        double seconds, hours;
        seconds = milliseconds / 1000;
        hours = seconds / 3600;
        return hours;
    }

    private static String calculateDistance() {
        String distanceTravelledInLast = "0";
        double timeInHr = convertTime(CarData.TIMER_DELAY);
        distanceTravelledInLast = new DecimalFormat("##.#").format(averageSpeed * timeInHr);
        return distanceTravelledInLast;
    }
}
