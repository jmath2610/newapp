/*
 * COPY RIGHT INFORMATION
 * ---------------------------
 * Mr.Jeevan Mathew ,
 * Kulathapappallil House ,
 * Kadakkad ,
 * Pandalam PO 689501, Kerala ,
 * Email ID : jmathew@gmx.de 
 *         AND
 * UVJ Technologies,
 * 1st Floor Thapasya Building , 
 * Infopark , Kochi,
 * Kerala,India ,682042  
 * CIN – U72200KL2003PTC016041 
 * Export code IEC1004008490 
 * retain all ownership and intellectual property rights in the code accompanying this message and in all derivatives hereto.
 *
 * REVISION HISTORY
 * ---------------------------
 * @author   : Arjun K
 * @since    : 22/03/2018
 * @comments :
 *
 * ---------------------------
 *
 * DESCRIPTION
 * ---------------------------
 *
 *
 *
 * ---------------------------
 */
package com.jeevan.virtualcockpit.aiflux;

import android.util.Log;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
//import org.apache.log4j.Logger;

public class FluctuationMonitor {
    private static final String TAG = FluctuationMonitor.class.getName();

    long delay = CarData.TIMER_DELAY;
    long alertDelay = 8000;
    Monitor monitorTask = new Monitor();
    AnalyseDriveBehaviour driveBehaviour = new AnalyseDriveBehaviour();
    Timer timer = new Timer();
    private volatile boolean isRunning = false;

    public boolean isRunning() {
        return isRunning;
    }

    public void start() {
        Date executionDate = new Date();
        timer.scheduleAtFixedRate(monitorTask, executionDate, delay);
        timer.scheduleAtFixedRate(driveBehaviour, executionDate, delay);
    }

    public void stop() {
        timer.cancel();
        isRunning = false;
    }

    private class Monitor extends TimerTask {
        private volatile Thread Monitor;

        //Runnable Monitor = this;

        public void stop() {
            Monitor = null;
        }

        @Override
        public void run() {
                try {
                    isRunning = true;
                    Log.d(TAG, " fluxmon monitor runs.... " );
                    int valueArray[] = DataAnalysis.analyseFluctuation(CarData.fluctuations);
                    if (valueArray.length > 0) {
                        if (valueArray[0] > CarData.ALERT_POINT) {
                            CarData.abnormalSpeedUp = true;
                            // Todo: sleep nicht gut
                            Thread.sleep(alertDelay);
                            CarData.abnormalSpeedUp = false;
                        } else if (valueArray[1] > CarData.ALERT_POINT) {
                            CarData.abnormalSpeedDown = true;
                            Thread.sleep(alertDelay);
                            CarData.abnormalSpeedDown = false;
                        }
                    } else {
                        CarData.abnormalSpeedUp = false;
                        CarData.abnormalSpeedDown = false;
                    }
                    if (CarData.fluctuations != null) {
                        CarData.fluctuations.clear();
                    }
                } catch (InterruptedException e) {
                    Log.d(TAG, " fluxmon monitor stopping due to interrupt.... " );
                    stop();
                    isRunning = false;
                    //Logger.getLogger(FluctuationMonitor.class).error(e.toString());
                }


        }
    }
    
    private class AnalyseDriveBehaviour extends TimerTask{

        private volatile Thread AnalyseDriveBehaviour;

        //Runnable Monitor = this;

        public void stop() {
            AnalyseDriveBehaviour = null;
        }

        @Override
        public void run() {

            try {
                Log.d(TAG, " fluxmon AnalyseDrivingBehaviour runs.... " );
                CarData.driveScore = DataAnalysis.calculateDriveScore(CarData.driveSpeeds, CarData.driveTorques, delay);
                Log.d(TAG, " drivescore -------------------------------------> :  " + CarData.driveScore );
                Thread.sleep(alertDelay);
                CarData.driveScore = 0;
                if (CarData.driveSpeeds != null)
                    CarData.driveSpeeds.clear();
                if (CarData.driveTorques != null)
                    CarData.driveTorques.clear();
            } catch (InterruptedException e) {
                //Logger.getLogger(FluctuationMonitor.class).error(e.toString());
                Log.d(TAG, " fluxmon AnalyseDrivingBehaviour stopping due to interrupt.... " );
                stop();
                isRunning = false;
            }


        }
        
    }
}
