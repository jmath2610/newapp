/*

 * COPY RIGHT INFORMATION
 * ---------------------------
 * Authors:
 *
 * Mr.Jeevan Mathew ,
 * Kulathapappallil House ,
 * Kadakkad ,
 * Pandalam PO 689501, Kerala ,
 * Email ID : jmathew@gmx.de
 * aka "JM"

 * and

 * Tarek El-Sibay,
 * Am Golfplatz 15,
 * 24576 Bad Bramstedt,
 * Deutschland
 * aka "TES"

 * retain all ownership and intellectual property rights in the code accompanying this message and in all derivatives hereto.
 * Previously not existent features/mechanisms have been added, where the authors JM and TES definitively claim their rights
 * that thirds need to obtain there written consent for further use.
 * Once written consent is obtained this software and system is licensed under Apache License Version 2.0
 *
 * REVISION HISTORY
 * ---------------------------
 * @author   :
 * @since    :
 * @comments :
 *
 * ---------------------------
 *
 * DESCRIPTION
 * ---------------------------
 * This software has been originally taken and from

 * https://github.com/pires/android-obd-reader
 * https://github.com/pires/obd-java-api
 *
 *
 *
 * ---------------------------
 */

package com.jeevan.virtualcockpit.config;

import com.jeevan.virtualcockpit.obd.commands.ObdCommand;
import com.jeevan.virtualcockpit.obd.commands.control.DistanceMILOnCommand;
import com.jeevan.virtualcockpit.obd.commands.control.DtcNumberCommand;
import com.jeevan.virtualcockpit.obd.commands.control.EquivalentRatioCommand;
import com.jeevan.virtualcockpit.obd.commands.control.ModuleVoltageCommand;
import com.jeevan.virtualcockpit.obd.commands.control.TimingAdvanceCommand;
import com.jeevan.virtualcockpit.obd.commands.control.TroubleCodesCommand;
import com.jeevan.virtualcockpit.obd.commands.control.VinCommand;
import com.jeevan.virtualcockpit.obd.commands.engine.LoadCommand;
import com.jeevan.virtualcockpit.obd.commands.engine.MassAirFlowCommand;
import com.jeevan.virtualcockpit.obd.commands.engine.OilTempCommand;
import com.jeevan.virtualcockpit.obd.commands.engine.RPMCommand;
import com.jeevan.virtualcockpit.obd.commands.engine.RuntimeCommand;
import com.jeevan.virtualcockpit.obd.commands.engine.ThrottlePositionCommand;
import com.jeevan.virtualcockpit.obd.commands.fuel.AirFuelRatioCommand;
import com.jeevan.virtualcockpit.obd.commands.fuel.ConsumptionRateCommand;
import com.jeevan.virtualcockpit.obd.commands.fuel.FindFuelTypeCommand;
import com.jeevan.virtualcockpit.obd.commands.fuel.FuelLevelCommand;
import com.jeevan.virtualcockpit.obd.commands.fuel.FuelTrimCommand;
import com.jeevan.virtualcockpit.obd.commands.fuel.WidebandAirFuelRatioCommand;
import com.jeevan.virtualcockpit.obd.commands.pressure.BarometricPressureCommand;
import com.jeevan.virtualcockpit.obd.commands.pressure.FuelPressureCommand;
import com.jeevan.virtualcockpit.obd.commands.pressure.FuelRailPressureCommand;
import com.jeevan.virtualcockpit.obd.commands.pressure.IntakeManifoldPressureCommand;
import com.jeevan.virtualcockpit.obd.commands.temperature.AirIntakeTemperatureCommand;
import com.jeevan.virtualcockpit.obd.commands.temperature.AmbientAirTemperatureCommand;
import com.jeevan.virtualcockpit.obd.commands.temperature.EngineCoolantTemperatureCommand;
import com.jeevan.virtualcockpit.obd.enums.FuelTrim;

import java.util.ArrayList;

/**
 * TODO put description
 */
public final class ObdConfig2 {

    public static ArrayList<ObdCommand> getCommands() {
        ArrayList<ObdCommand> cmds = new ArrayList<>();

        // Control
        cmds.add(new ModuleVoltageCommand());
        cmds.add(new EquivalentRatioCommand());
        cmds.add(new DistanceMILOnCommand()); //------- problem *
        cmds.add(new DtcNumberCommand());
        cmds.add(new TimingAdvanceCommand());
        cmds.add(new TroubleCodesCommand());
        cmds.add(new VinCommand());

        // Engine
        cmds.add(new LoadCommand());
        cmds.add(new RPMCommand());
        cmds.add(new RuntimeCommand());
        cmds.add(new MassAirFlowCommand());
        cmds.add(new ThrottlePositionCommand());

        // Fuel
        cmds.add(new FindFuelTypeCommand());
        cmds.add(new ConsumptionRateCommand()); //------- problem ???
        //cmds.add(new AverageFuelEconomyObdCommand());
        //cmds.add(new FuelEconomyCommand());
        cmds.add(new FuelLevelCommand());
        // cmds.add(new FuelEconomyMAPObdCommand());
        // cmds.add(new FuelEconomyCommandedMAPObdCommand());
        cmds.add(new FuelTrimCommand(FuelTrim.LONG_TERM_BANK_1));
        cmds.add(new FuelTrimCommand(FuelTrim.LONG_TERM_BANK_2));
        cmds.add(new FuelTrimCommand(FuelTrim.SHORT_TERM_BANK_1));
        cmds.add(new FuelTrimCommand(FuelTrim.SHORT_TERM_BANK_2));
        cmds.add(new AirFuelRatioCommand());
        cmds.add(new WidebandAirFuelRatioCommand());
        cmds.add(new OilTempCommand()); //------- problem ???

        // Pressure
        cmds.add(new BarometricPressureCommand());
        cmds.add(new FuelPressureCommand()); //------- problem
        cmds.add(new FuelRailPressureCommand());
        cmds.add(new IntakeManifoldPressureCommand());

        // Temperature
        cmds.add(new AirIntakeTemperatureCommand());
        cmds.add(new AmbientAirTemperatureCommand());
        cmds.add(new EngineCoolantTemperatureCommand());

        // Misc
        //cmds.add(new SpeedCommand());
        return cmds;
    }

}
