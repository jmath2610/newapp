/*
 * COPY RIGHT INFORMATION
 * ---------------------------
 * Mr.Jeevan Mathew ,
 * Kulathapappallil House ,
 * Kadakkad ,
 * Pandalam PO 689501, Kerala ,
 * Email ID : jmathew@gmx.de 
 *         AND
 * UVJ Technologies,
 * 1st Floor Thapasya Building , 
 * Infopark , Kochi,
 * Kerala,India ,682042  
 * CIN – U72200KL2003PTC016041 
 * Export code IEC1004008490 
 * retain all ownership and intellectual property rights in the code accompanying this message and in all derivatives hereto.
 *
 * REVISION HISTORY
 * ---------------------------
 * @author   :
 * @since    :
 * @comments :
 *
 * ---------------------------
 *
 * DESCRIPTION
 * ---------------------------
 *
 *
 *
 * ---------------------------
 */
package com.jeevan.virtualcockpit.aiflux;



//import com.jeevan.virtualcockpit.controller.Resources;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
//import javafx.scene.Node;
//import javafx.scene.control.ProgressBar;
//import javafx.scene.paint.Color;



public class CarData {

    public static boolean fullScreenStage = false;
    //public static String SCENE_LIST[] = {"ClusterDigital.fxml", "ClusterAnalog_1.fxml"};
    public static String DRIVER_NAME;
    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final String TIME_FORMAT = "hh:mm";
    public static final String TIME_FORMAT_A = "a";
    public static final int SPEED_LIMIT = 60;
    //public static final Color ALERT_COLOR = Color.RED;
    //public static final Color ECONOMY_COLOR = Color.GREEN;
    //public static final Color NORMAL_COLOR = Color.WHITE;
    public static int CURRENT_SCENE = 1;
    public static int RPM_DIVIDER = 8000 / 240;
    public static String RSI_SERVER_HOST;
    public static int RSI_SERVER_PORT;
    public static final String DEGREE = "\u00b0";
    /**
     * *****For Speed fluctuation monitoring******
     * * From earlier application.properties file under first version of VirtualCockpit from 2018
     *      * /*#Properties for speed fluctuation monitoring.
     *      *     #Change these parameteres to increase/decrease the fluctuation monitoring.
     *      *             #Give delay in milleseconds
     *      *     vc.delay=30000
     *      *     vc.window.blockSize=4
     *      *     vc.alertPoint=5
     *      *     vc.differenceLimit=10
     *      *
     *
     * How Speed fluctuation monitoring works.
     * The application monitors the variation/change in the speed for a fixed
     * interval of time and analyses the data using a sliding window algorithm.
     *
     * Parameters from the application.properties file indicates,
     * o vc.delay --- Interval of execution of speed monitoring task.
     * Eg: If delay 6000(mill.seconds) means the interval will be ONE
     * MINUTE (programme will multiply this value with 10).
     * Delay = 3000 means interval will be HALF MINUTE and so on.
     * o That is the algorithm will execute in each one minute and analyses the
     * speed variations happened in the last minute.
     * o vc.window.blockSize --- This is the block size for Sliding window
     * algorithm.
     * ---Sliding Window Algorithm
     * Sliding window algorithm accepts an array of data and breaks
     * the array with a fixed length of elements called blocks and
     * performs some logic on these blocks.
     * In Virtual Cockpit application, input for this algorithm is an
     * arraylist containing fluctuated speed from the last interval.
     * It then breaks this arraylist with
     * block size
     * vc.window.blockSize.
     * After it finds the maximum of each block, stores in an array list
     * and comparing adjacent values.
     * If that difference is greater than 20 or less than -20 (currently
     * given a fixed threshold) increases a counter variable.
     * If the counter variable > vc.alertPoint, then assuming that there
     * happened a rash driving in the last interval and alerts the user
     * accordingly.
     * NB: For real scenario we should also consider other external factors like Road condition,
     * vehicle conditions etc to implement this monitoring.
     */

    public static List<Integer> fluctuations = new ArrayList<>();
    public static boolean abnormalSpeedUp = false;
    public static boolean abnormalSpeedDown = false;
    public static int TIMER_DELAY = 4000;
    public static int WINDOW_BLOCK_SIZE = 4;
    public static int ALERT_POINT = 4;
    public static int DIFF_LIMIT = 10;
    public static String DISTANCE_TRAVELLED;



    /**
     * *******************************************
     */
    /**
     * **Drive Score Analysis Testing***
     */
    public static List<Integer> driveSpeeds = new ArrayList<>();
    public static List<Integer> driveTorques = new ArrayList<>();
    public static int optimalSpeed = 100;
    public static int optimalRPM = 3000; //1500 for Diesel
    public static int speedWeightage = 50; // A fixed value for learning an can be changed later for tuning.
    public static int rpmWeightage = 50; // A fixed value for learning an can be changed later for tuning.
    public static int driveScore = 0; // Drive score obtained in an interval.
    // Emojis
    /*
    public static Map<String, String> EMOJI = new ConcurrentHashMap<String, String>() {
        {
            put("awesome", Resources.EMOJI + "awesome.png");
            put("great", Resources.EMOJI + "great.png");
            put("good", Resources.EMOJI + "good.png");
            put("warning", Resources.EMOJI + "warning.png");
            put("danger", Resources.EMOJI + "danger.png");
            put("tooDanger", Resources.EMOJI + "too_danger.png");
        }
    };
*/
    /**
     * *********************************
     */
/*    public static enum OBDType {
        SPEED,
        RPM,
        EL,
        ECT
    };*/

    public static enum CockpitSymbols {
        HEAD_LIGHT,
        LEFT_TURN,
        RIGHT_TURN,
        HAND_BRAKE,
        SPEED_NEEDLE_COLOR,
        TACHO_NEEDLE_COLOR,
        SEAT_BELT,
        CLIMATE_CONTROl,
        FOG_LAMP,
        WIPER,
        HEATER
    };

    public static final List<String> MENU_ITEMS = new ArrayList<String>() {
        {
            add("Profiles");
            add("Mode");
            add("Climate Controll");
            add("Entertainment");
            add("Phone");
            add("Schedule");
            add("Map");
        }
    };
    //public static final Map<String, Node> COMPONENTS = new ConcurrentHashMap<String, Node>();
    //public static final Map<String, ProgressBar> FUEL_TEMP = new LinkedHashMap<String, ProgressBar>();
    public static final List<String> ALERT_LIST_LEFT = new ArrayList<String>() {
        {
            add("Call Ralph by 2.00 pm");
            add("Call Hugh by 3.00 pm");
            add("Meeting at 1.00 pm");
        }
    };
    public static final List<String> ALERT_LIST_RIGHT = new ArrayList<String>() {
        {
            add("Change oil in next 2000 km");
            add("Battery needs to check");
        }
    };
    /**
     * ******************Dynamic Controls************************
     */
    public static Map<String, String> NEEDLE_COLOR = new ConcurrentHashMap<String, String>() {
        {
            put("speedNeedleColor", "DEFAULT");
            put("tachoNeedleColor", "DEFAULT");
        }
    };
    public static Map<String, Boolean> COMPONENT_STATES = new ConcurrentHashMap<String, Boolean>() {
        {
            put("leftTurn", false);
            put("rightTurn", false);
            put("handBrake", false);
            put("seatBelt", false);
            put("headLamp", false);
            put("climate", false);
            put("heater", false);
            put("fogLamp", false);
            put("wiper", false);
        }
    };

    public static void updateKeyValue(CockpitSymbols component) {
        switch (component) {
            case HEAD_LIGHT:
                COMPONENT_STATES.put("headLamp", (COMPONENT_STATES.get("headLamp") == true ? false : true));
                break;
            case LEFT_TURN:
                COMPONENT_STATES.put("rightTurn", Boolean.FALSE);
                COMPONENT_STATES.put("leftTurn", (COMPONENT_STATES.get("leftTurn") == true ? false : true));
                break;
            case RIGHT_TURN:
                COMPONENT_STATES.put("leftTurn", Boolean.FALSE);
                COMPONENT_STATES.put("rightTurn", (COMPONENT_STATES.get("rightTurn") == true ? false : true));
                break;
            case HAND_BRAKE:
                COMPONENT_STATES.put("handBrake", (COMPONENT_STATES.get("handBrake") == true ? false : true));
                break;
            case SEAT_BELT:
                COMPONENT_STATES.put("seatBelt", (COMPONENT_STATES.get("seatBelt") == true ? false : true));
                break;
            case CLIMATE_CONTROl:
                COMPONENT_STATES.put("climate", (COMPONENT_STATES.get("climate") == true ? false : true));
                break;
            case HEATER:
                COMPONENT_STATES.put("heater", (COMPONENT_STATES.get("heater") == true ? false : true));
                break;
            case FOG_LAMP:
                COMPONENT_STATES.put("fogLamp", (COMPONENT_STATES.get("fogLamp") == true ? false : true));
                break;
            case WIPER:
                COMPONENT_STATES.put("wiper", (COMPONENT_STATES.get("wiper") == true ? false : true));
                break;
            default:
                break;
        }
    }

    public static void updateKeyValue(CockpitSymbols component, String color) {
        switch (component) {
            case SPEED_NEEDLE_COLOR:
                NEEDLE_COLOR.put("speedNeedleColor", (color.equals("0xffffffff") ? "DEFAULT" : color));
                break;
            case TACHO_NEEDLE_COLOR:
                NEEDLE_COLOR.put("tachoNeedleColor", (color.equals("0xffffffff") ? "DEFAULT" : color));
                break;
            default:
                break;
        }
    }
    /**
     * **********************************
     */
}
