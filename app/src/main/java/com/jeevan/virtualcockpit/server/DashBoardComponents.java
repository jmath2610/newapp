/*

 * COPY RIGHT INFORMATION
 * ---------------------------
 * Authors:
 *
 * Mr.Jeevan Mathew ,
 * Kulathapappallil House ,
 * Kadakkad ,
 * Pandalam PO 689501, Kerala ,
 * Email ID : jmathew@gmx.de
 * aka "JM"

 * and

 * Tarek El-Sibay,
 * Am Golfplatz 15,
 * 24576 Bad Bramstedt,
 * Deutschland
 * aka "TES"

 * retain all ownership and intellectual property rights in the code accompanying this message and in all derivatives hereto.
 * Previously not existent features/mechanisms have been added, where the authors JM and TES definitively claim their rights
 * that thirds need to obtain there written consent for further use.
 * Once written consent is obtained this software and system is licensed under Apache License Version 2.0
 *
 * REVISION HISTORY
 * ---------------------------
 * @author   :
 * @since    :
 * @comments :
 *
 * ---------------------------
 *
 * DESCRIPTION
 * ---------------------------
 * This software has been originally taken and from

 * https://github.com/pires/android-obd-reader
 * https://github.com/pires/obd-java-api
 *
 *
 *
 * ---------------------------
 */
package com.jeevan.virtualcockpit.server;

import java.util.Map;

public class DashBoardComponents {
        private String speedNeedleColor = "DEFAULT";
        private String tachoNeedleColor = "DEFAULT";
        private Boolean leftTurn = false;
        private Boolean rightTurn = false;
        private Boolean handBrake = false;
        private Boolean seatBelt = false;
        private Boolean headLamp = false;
        private Boolean climate = false;
        private Boolean heater = false;
        private Boolean wiper = false;
        private Boolean fogLamp = false;

        public DashBoardComponents(Map<String,String> NEEDLE_COLOR, Map<String,Boolean> COMPONENT_STATES) {
            speedNeedleColor = NEEDLE_COLOR.get("speedNeedleColor");
            tachoNeedleColor = NEEDLE_COLOR.get("tachoNeedleColor");
            leftTurn = COMPONENT_STATES.get("leftTurn");
            rightTurn = COMPONENT_STATES.get("rightTurn");
            handBrake = COMPONENT_STATES.get("handBrake");
            seatBelt = COMPONENT_STATES.get("seatBelt");
            headLamp = COMPONENT_STATES.get("headLamp");
            climate = COMPONENT_STATES.get("climate");
            heater = COMPONENT_STATES.get("heater");
            fogLamp = COMPONENT_STATES.get("fogLamp");
            wiper = COMPONENT_STATES.get("wiper");
        }
        public String getSpeedNeedleColor() {
            return speedNeedleColor;
        }

        public void setSpeedNeedleColor(String speedNeedleColor) {
            this.speedNeedleColor = speedNeedleColor;
        }

        public String getTachoNeedleColor() {
            return tachoNeedleColor;
        }

        public void setTachoNeedleColor(String tachoNeedleColor) {
            this.tachoNeedleColor = tachoNeedleColor;
        }

        public Boolean getLeftTurn() {
            return leftTurn;
        }

        public void setLeftTurn(Boolean leftTurn) {
            this.leftTurn = leftTurn;
        }

        public Boolean getRightTurn() {
            return rightTurn;
        }

        public void setRightTurn(Boolean rightTurn) {
            this.rightTurn = rightTurn;
        }

        public Boolean getHandBrake() {
            return handBrake;
        }

        public void setHandBrake(Boolean handBrake) {
            this.handBrake = handBrake;
        }

        public Boolean getSeatBelt() {
            return seatBelt;
        }

        public void setSeatBelt(Boolean seatBelt) {
            this.seatBelt = seatBelt;
        }

        public Boolean getHeadLamp() {
            return headLamp;
        }

        public void setHeadLamp(Boolean headLamp) {
            this.headLamp = headLamp;
        }

        public Boolean getClimate() {
            return climate;
        }

        public void setClimate(Boolean climate) {
            this.climate = climate;
        }

        public Boolean getHeater() {
            return heater;
        }

        public void setHeater(Boolean heater) {
            this.heater = heater;
        }

        public Boolean getWiper() {
            return wiper;
        }

        public void setWiper(Boolean wiper) {
            this.wiper = wiper;
        }

        public Boolean getFogLamp() {
            return fogLamp;
        }

        public void setFogLamp(Boolean fogLamp) {
            this.fogLamp = fogLamp;
        }
}


