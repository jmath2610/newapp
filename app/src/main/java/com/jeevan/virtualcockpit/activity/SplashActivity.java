/*

 * COPY RIGHT INFORMATION
 * ---------------------------
 * Authors:
 *
 * Mr.Jeevan Mathew ,
 * Kulathapappallil House ,
 * Kadakkad ,
 * Pandalam PO 689501, Kerala ,
 * Email ID : jmathew@gmx.de
 * aka "JM"

 * and

 * Tarek El-Sibay,
 * Am Golfplatz 15,
 * 24576 Bad Bramstedt,
 * Deutschland
 * aka "TES"

 * retain all ownership and intellectual property rights in the code accompanying this message and in all derivatives hereto.
 * Previously not existent features/mechanisms have been added, where the authors JM and TES definitively claim their rights
 * that thirds need to obtain there written consent for further use.
 * Once written consent is obtained this software and system is licensed under Apache License Version 2.0
 *
 * REVISION HISTORY
 * ---------------------------
 * @author   :
 * @since    :
 * @comments :
 *
 * ---------------------------
 *
 * DESCRIPTION
 * ---------------------------
 * This software has been originally taken and from

 * https://github.com/pires/android-obd-reader
 * https://github.com/pires/obd-java-api
 *
 *
 *
 * ---------------------------
 */

package com.jeevan.virtualcockpit.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.jeevan.virtualcockpit.R;

public class SplashActivity extends AppCompatActivity {
    /*private final int SPLASH_DISPLAY_LENGTH = 8000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        *//* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*//*
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                *//* Create an Intent that will start the Menu-Activity. *//*
                Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }*/
}
