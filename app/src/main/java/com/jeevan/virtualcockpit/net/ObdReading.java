/*

 * COPY RIGHT INFORMATION
 * ---------------------------
 * Authors:
 *
 * Mr.Jeevan Mathew ,
 * Kulathapappallil House ,
 * Kadakkad ,
 * Pandalam PO 689501, Kerala ,
 * Email ID : jmathew@gmx.de
 * aka "JM"

 * and

 * Tarek El-Sibay,
 * Am Golfplatz 15,
 * 24576 Bad Bramstedt,
 * Deutschland
 * aka "TES"

 * retain all ownership and intellectual property rights in the code accompanying this message and in all derivatives hereto.
 * Previously not existent features/mechanisms have been added, where the authors JM and TES definitively claim their rights
 * that thirds need to obtain there written consent for further use.
 * Once written consent is obtained this software and system is licensed under Apache License Version 2.0
 *
 * REVISION HISTORY
 * ---------------------------
 * @author   :
 * @since    :
 * @comments :
 *
 * ---------------------------
 *
 * DESCRIPTION
 * ---------------------------
 * This software has been originally taken and from

 * https://github.com/pires/android-obd-reader
 * https://github.com/pires/obd-java-api
 *
 *
 *
 * ---------------------------
 */


package com.jeevan.virtualcockpit.net;

import java.util.HashMap;
import java.util.Map;



/**
 * DTO for OBD readings.
 */
public class ObdReading {
    private double latitude, longitude, altitude;
    private long timestamp;
    private String vin; // vehicle id
    private Map<String, String> readings;

    public ObdReading() {
        readings = new HashMap<>();
    }

    public ObdReading(double latitude, double longitude, double altitude, long timestamp,
                      String vin, Map<String, String> readings) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.timestamp = timestamp;
        this.vin = vin;
        this.readings = readings;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vehicleid) {
        this.vin = vehicleid;
    }

    public Map<String, String> getReadings() {
        return readings;
    }

    public void setReadings(Map<String, String> readings) {
        this.readings = readings;
    }

    public String toString() {

        return "lat:" + latitude + ";" +
                "long:" + longitude + ";" +
                "alt:" + altitude + ";" +
                "vin:" + vin + ";" +
                "readings:" + readings.toString().substring(10).replace("}", "").replace(",", ";");
    }

}
