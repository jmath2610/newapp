/*

 * COPY RIGHT INFORMATION
 * ---------------------------
 * Authors:
 *
 * Mr.Jeevan Mathew ,
 * Kulathapappallil House ,
 * Kadakkad ,
 * Pandalam PO 689501, Kerala ,
 * Email ID : jmathew@gmx.de
 * aka "JM"

 * and

 * Tarek El-Sibay,
 * Am Golfplatz 15,
 * 24576 Bad Bramstedt,
 * Deutschland
 * aka "TES"

 * retain all ownership and intellectual property rights in the code accompanying this message and in all derivatives hereto.
 * Previously not existent features/mechanisms have been added, where the authors JM and TES definitively claim their rights
 * that thirds need to obtain there written consent for further use.
 * Once written consent is obtained this software and system is licensed under Apache License Version 2.0
 *
 * REVISION HISTORY
 * ---------------------------
 * @author   :
 * @since    :
 * @comments :
 *
 * ---------------------------
 *
 * DESCRIPTION
 * ---------------------------
 * This software has been originally taken and from

 * https://github.com/pires/android-obd-reader
 * https://github.com/pires/obd-java-api
 *
 *
 *
 * ---------------------------
 */

package com.jeevan.virtualcockpit.activity;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.jeevan.virtualcockpit.R;
import com.jeevan.virtualcockpit.aiflux.CarData;
import com.jeevan.virtualcockpit.aiflux.FluctuationMonitor;
import com.jeevan.virtualcockpit.config.ObdConfig;

import com.jeevan.virtualcockpit.config.ObdConfig2;
import com.jeevan.virtualcockpit.io.AbstractGatewayService;
import com.jeevan.virtualcockpit.io.LogCSVWriter;
import com.jeevan.virtualcockpit.io.MockObdGatewayService;
import com.jeevan.virtualcockpit.io.ObdCommandJob;

import com.jeevan.virtualcockpit.io.ObdGatewayService;
import com.jeevan.virtualcockpit.io.Permissons;
import com.jeevan.virtualcockpit.net.ObdReading;
import com.jeevan.virtualcockpit.net.ObdService;
import com.jeevan.virtualcockpit.server.JSONData;
import com.jeevan.virtualcockpit.server.RSIServerData;
import com.jeevan.virtualcockpit.obd.commands.ObdCommand;
import com.jeevan.virtualcockpit.obd.commands.SpeedCommand;
import com.jeevan.virtualcockpit.obd.commands.engine.RPMCommand;
import com.jeevan.virtualcockpit.obd.commands.engine.RuntimeCommand;
import com.jeevan.virtualcockpit.obd.commands.fuel.FuelLevelCommand;
import com.jeevan.virtualcockpit.obd.enums.AvailableCommandNames;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.inject.Inject;
import com.google.android.gms.location.LocationListener;


import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import roboguice.RoboGuice;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import android.app.Activity;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import com.jeevan.virtualcockpit.io.ObdProgressListener;
import com.jeevan.virtualcockpit.trips.TripLog;
import com.jeevan.virtualcockpit.trips.TripRecord;
import com.jeevan.virtualcockpit.trips.TripRecord2;
import com.jeevan.virtualcockpit.io.Permissons;

@ContentView(R.layout.main)
public class MainActivity extends RoboActivity implements ObdProgressListener, LocationListener, GpsStatus.Listener {

    private static final String TAG = MainActivity.class.getName();
    private static final int NO_BLUETOOTH_ID = 0;
    private static final int BLUETOOTH_DISABLED = 1;
    private static final int START_LIVE_DATA = 2;
    private static final int START_STAND_DATA = 2222;
    private static final int STOP_LIVE_DATA = 3;
    private static final int SETTINGS = 4;
    private static final int GET_DTC = 5;
    private static final int TABLE_ROW_MARGIN = 7;
    private static final int NO_ORIENTATION_SENSOR = 8;
    private static final int NO_GPS_SUPPORT = 9;
    private static final int TRIPS_LIST = 10;
    private static final int SAVE_TRIP_NOT_AVAILABLE = 11;
    private static final int REQUEST_ENABLE_BT = 1234;
    private static boolean bluetoothDefaultIsEnable = false;

    private RSIServerData rsiServerData =null;
    private static Socket socket=null;
    private static final String SERVER_IP = "rsi.car-assistant.de";
    private static final int SERVERPORT = 8040;//4000;
    private DataOutputStream clientOutput = null;

    static {
        RoboGuice.setUseAnnotationDatabases(false);
    }
    private double fusedLatitude = 0.0;
    private  double fusedLongitude = 0.0;
    String speed_old = "0";
    String rpm_old = "0";
    String str_driver_name;
    public Map<String, String> commandResult = new HashMap<String, String>();
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    boolean mGpsIsStarted = false;
    private LocationProvider mLocProvider;
    //private LogCSVWriter myCSVWriter;
    private Location mLastLocation;
    /// the trip log
    private TripLog triplog;
    private TripRecord currentTrip;

    @InjectView(R.id.compass_text)
    private TextView compass;
    public FluctuationMonitor fm;

    private final SensorEventListener orientListener = new SensorEventListener() {

        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0];
            String dir = "";
            if (x >= 337.5 || x < 22.5) {
                dir = "N";
            } else if (x >= 22.5 && x < 67.5) {
                dir = "NE";
            } else if (x >= 67.5 && x < 112.5) {
                dir = "E";
            } else if (x >= 112.5 && x < 157.5) {
                dir = "SE";
            } else if (x >= 157.5 && x < 202.5) {
                dir = "S";
            } else if (x >= 202.5 && x < 247.5) {
                dir = "SW";
            } else if (x >= 247.5 && x < 292.5) {
                dir = "W";
            } else if (x >= 292.5 && x < 337.5) {
                dir = "NW";
            }
            updateTextView(compass, dir);
        }
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // do nothing
        }
    };

    @InjectView(R.id.BT_STATUS)
    private TextView btStatusTextView;
    @InjectView(R.id.OBD_STATUS)
    private TextView obdStatusTextView;
    @InjectView(R.id.GPS_POS)
    private TextView gpsStatusTextView;
    @InjectView(R.id.vehicle_view)
    private LinearLayout vv;
    @InjectView(R.id.data_table)
    private TableLayout tl;
    @Inject
    private SensorManager sensorManager;
    @Inject
    private PowerManager powerManager;
    @Inject
    private SharedPreferences prefs;
    private boolean isServiceBound;
    private AbstractGatewayService service;

    public int mqueuecounter = 0;

    private final Runnable mQueueCommands = new Runnable() {
        public void run() {
            Log.d(TAG, "Runnable mQueueCommands before queuecommands" );
            if (service != null && service.isRunning() && service.queueEmpty()) {
                queueCommands();

                double lat = 0;
                double lon = 0;
                double alt = 0;
                final int posLen = 7;
                if (mGpsIsStarted && mLastLocation != null) {
                    lat = mLastLocation.getLatitude();
                    lon = mLastLocation.getLongitude();
                    alt = mLastLocation.getAltitude();

                    StringBuilder sb = new StringBuilder();
                    sb.append("Lat: ");
                    sb.append(String.valueOf(mLastLocation.getLatitude()).substring(0, posLen));
                    sb.append(" Lon: ");
                    sb.append(String.valueOf(mLastLocation.getLongitude()).substring(0, posLen));
                    sb.append(" Alt: ");
                    sb.append(String.valueOf(mLastLocation.getAltitude()));
                    gpsStatusTextView.setText(sb.toString());
                }
                /*if (prefs.getBoolean(ConfigActivity.UPLOAD_DATA_KEY, false)) {
                    // Upload the current reading by http
                    final String vin = prefs.getString(ConfigActivity.VEHICLE_ID_KEY, "UNDEFINED_VIN");
                    Map<String, String> temp = new HashMap<String, String>();
                    temp.putAll(commandResult);
                    ObdReading reading = new ObdReading(lat, lon, alt, System.currentTimeMillis(), vin, temp);
                    new UploadAsyncTask().execute(reading);

                } else if (prefs.getBoolean(ConfigActivity.ENABLE_FULL_LOGGING_KEY, false)) {
                    // Write the current reading to CSV
                    final String vin = prefs.getString(ConfigActivity.VEHICLE_ID_KEY, "UNDEFINED_VIN");
                    Map<String, String> temp = new HashMap<String, String>();
                    temp.putAll(commandResult);
                    ObdReading reading = new ObdReading(lat, lon, alt, System.currentTimeMillis(), vin, temp);
                    if(reading != null) myCSVWriter.writeLineCSV(reading);
                }*/
                commandResult.clear();
            }

            if(mqueuecounter2 == 0 && mqueuecounter == 0) {
                new Handler().postDelayed(mQueueCommands, (ConfigActivity.getObdUpdatePeriod(prefs)));
            }
        }
    };

    public int mqueuecounter2 = 0;

    private final Runnable mQueueCommands2 = new Runnable() {
        public void run() {
            if (service != null && service.isRunning() && service.queueEmpty2()) {
                Log.d(TAG, "Runnable mQueueCommands2 before queuecommands2" );
                queueCommands2();

                double lat = 0;
                double lon = 0;
                double alt = 0;
                final int posLen = 7;
                if (mGpsIsStarted && mLastLocation != null) {
                    lat = mLastLocation.getLatitude();
                    lon = mLastLocation.getLongitude();
                    alt = mLastLocation.getAltitude();

                    StringBuilder sb = new StringBuilder();
                    sb.append("Lat: ");
                    sb.append(String.valueOf(mLastLocation.getLatitude()).substring(0, posLen));
                    sb.append(" Lon: ");
                    sb.append(String.valueOf(mLastLocation.getLongitude()).substring(0, posLen));
                    sb.append(" Alt: ");
                    sb.append(String.valueOf(mLastLocation.getAltitude()));
                    gpsStatusTextView.setText(sb.toString());
                }
                /*if (prefs.getBoolean(ConfigActivity.UPLOAD_DATA_KEY, false)) {
                    // Upload the current reading by http
                    final String vin = prefs.getString(ConfigActivity.VEHICLE_ID_KEY, "UNDEFINED_VIN");
                    Map<String, String> temp = new HashMap<String, String>();
                    temp.putAll(commandResult);
                    ObdReading reading = new ObdReading(lat, lon, alt, System.currentTimeMillis(), vin, temp);
                    new UploadAsyncTask().execute(reading);

                } else if (prefs.getBoolean(ConfigActivity.ENABLE_FULL_LOGGING_KEY, false)) {
                    // Write the current reading to CSV
                    final String vin = prefs.getString(ConfigActivity.VEHICLE_ID_KEY, "UNDEFINED_VIN");
                    Map<String, String> temp = new HashMap<String, String>();
                    temp.putAll(commandResult);
                    ObdReading reading = new ObdReading(lat, lon, alt, System.currentTimeMillis(), vin, temp);
                    if(reading != null) myCSVWriter.writeLineCSV(reading);
                }*/
                commandResult.clear();
            }
            // run again in period defined in preferences
            mqueuecounter2++;
            if (mqueuecounter2 < 2){
            new Handler().postDelayed(mQueueCommands2, ConfigActivity.getObdUpdatePeriod(prefs)*2);
        }}
    };


    private Sensor orientSensor = null;
    private PowerManager.WakeLock wakeLock = null;
    private boolean preRequisites = true;
    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            Log.d(TAG, className.toString() + " service is bound");
            isServiceBound = true;
            service = ((AbstractGatewayService.AbstractGatewayServiceBinder) binder).getService();
            service.setContext(MainActivity.this);
            Log.d(TAG, "Starting live data");
            try {
                service.startService();
                if (preRequisites)
                    btStatusTextView.setText(getString(R.string.status_bluetooth_connected));
            } catch (IOException ioe) {
                Log.e(TAG, "Failure Starting live data");
                btStatusTextView.setText(getString(R.string.status_bluetooth_error_connecting));
                doUnbindService();
            }
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        // This method is *only* called when the connection to the service is lost unexpectedly
        // and *not* when the client unbinds (http://developer.android.com/guide/components/bound-services.html)
        // So the isServiceBound attribute should also be set to false when we unbind from the service.
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(TAG, className.toString() + " service is unbound");
            isServiceBound = false;
        }
    };

    public static String LookUpCommand(String txt) {
        for (AvailableCommandNames item : AvailableCommandNames.values()) {
            if (item.getValue().equals(txt)) return item.name();
        }
        return txt;
    }

    public void updateTextView(final TextView view, final String txt) {
        new Handler().post(new Runnable() {
            public void run() {
                view.setText(txt);
            }
        });
    }

    public void stateUpdate(final ObdCommandJob job) {
        final String cmdName = job.getCommand().getName();
        String cmdResult = "";
        final String cmdID = LookUpCommand(cmdName);

        if (job.getState().equals(ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR)) {
            cmdResult = job.getCommand().getResult();
            if (cmdResult != null && isServiceBound) {
                obdStatusTextView.setText(cmdResult.toLowerCase());
            }
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.BROKEN_PIPE)) {
            if (isServiceBound)
                stopLiveData();
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.NOT_SUPPORTED)) {
            cmdResult = getString(R.string.status_obd_no_support);
        } else {
            cmdResult = job.getCommand().getFormattedResult();
            if(isServiceBound)
                obdStatusTextView.setText(getString(R.string.status_obd_data));
        }

        if (vv.findViewWithTag(cmdID) != null) {
            TextView existingTV = (TextView) vv.findViewWithTag(cmdID);
            existingTV.setText(cmdResult);
        } else addTableRow(cmdID, cmdName, cmdResult);
        commandResult.put(cmdID, cmdResult);
        updateTripStatistic(job, cmdID);
        if(fm !=null && !fm.isRunning())
        {
            fm.stop();
            fm = null;
            fm = new FluctuationMonitor();
            Log.d(TAG, "restart fluxmon  " );
            fm.start();
        }

            if(cmdResult != null && !cmdResult.equals("NODATA")) {
                Log.d(TAG, "------------STARTING SERVER CONNECTION WITH cmdresult and cmdID and cmName ----------> " + cmdResult + " " + cmdID + " " + cmdName);
                new Thread(new ServerConnection(cmdID, cmdResult)).start();
            } else {

            }

    }


    private void updateTripStatistic(final ObdCommandJob job, final String cmdID) {

        if (currentTrip != null) {
            if (cmdID.equals(AvailableCommandNames.SPEED.toString())) {
                SpeedCommand command = (SpeedCommand) job.getCommand();
                currentTrip.setSpeedMax(command.getMetricSpeed());
            } else if (cmdID.equals(AvailableCommandNames.ENGINE_RPM.toString())) {
                RPMCommand command = (RPMCommand) job.getCommand();
                currentTrip.setEngineRpmMax(command.getRPM());
            } else if (cmdID.endsWith(AvailableCommandNames.ENGINE_RUNTIME.toString())) {
                RuntimeCommand command = (RuntimeCommand) job.getCommand();
                currentTrip.setEngineRuntime(command.getFormattedResult());
            }
        }
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d(TAG ,"Checking Permissions 1");
            checkPermission();
        }
        Log.d(TAG ,"Checking Permissions 2222222222222222222222222");
        //Permissons.Check_FINE_LOCATION(MainActivity.this);
        //checkPermission();

        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter != null)
            bluetoothDefaultIsEnable = btAdapter.isEnabled();

        // get Orientation sensor
        List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ORIENTATION);
        if (sensors.size() > 0)
            orientSensor = sensors.get(0);
        else
            showDialog(NO_ORIENTATION_SENSOR);

        // create a log instance for use by this application
        triplog = TripLog.getInstance(this.getApplicationContext());

        obdStatusTextView.setText(getString(R.string.status_obd_disconnected));

        if (checkPlayServices()) {
            startFusedLocation();
            registerRequestUpdate(this);
        }
    }

    public void registerRequestUpdate(final LocationListener listener) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000); // every second

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,  listener);
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!isGoogleApiClientConnected()) {
                        mGoogleApiClient.connect();
                    }
                    registerRequestUpdate(listener);
                }
            }
        }, 1000);
    }

    public boolean isGoogleApiClientConnected() {
        return mGoogleApiClient != null && mGoogleApiClient.isConnected();
    }

    private void startFusedLocation() {

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnectionSuspended(int cause) {
                        }

                        @Override
                        public void onConnected(Bundle connectionHint) {

                        }
                    }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {

                        @Override
                        public void onConnectionFailed(ConnectionResult result) {

                        }
                    }).build();
            mGoogleApiClient.connect();
        } else {
            mGoogleApiClient.connect();
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(),
                        "This device is supported. Please download google play services", Toast.LENGTH_LONG)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    protected static final int REQUEST_CHECK_SETTINGS = 0x1; //gekautes Kaugummi

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    private void checkPermission() {
        if(!Permissons.Check_STORAGE(MainActivity.this)){
            Permissons.Request_STORAGE(MainActivity.this,22);
        }

        displayLocationSettingsRequest(this.getApplicationContext());
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED

        ){//Can add more as per requirement
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    123);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "Entered onStart...");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseWakeLockIfHeld();
        if (isServiceBound) {
            doUnbindService();
        }
        endTrip();
        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter != null && btAdapter.isEnabled() && !bluetoothDefaultIsEnable)
            btAdapter.disable();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "Pausing..");
        releaseWakeLockIfHeld();
    }

    /**
     * If lock is held, release. Lock will be held when the service is running.
     */
    private void releaseWakeLockIfHeld() {
        if (wakeLock.isHeld())
            wakeLock.release();
    }

    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Resuming..");
        sensorManager.registerListener(orientListener, orientSensor,
                SensorManager.SENSOR_DELAY_UI);
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
                "ObdReader");

        // get Bluetooth device
        final BluetoothAdapter btAdapter = BluetoothAdapter
                .getDefaultAdapter();

        preRequisites = btAdapter != null && btAdapter.isEnabled();
        if (!preRequisites && prefs.getBoolean(ConfigActivity.ENABLE_BT_KEY, false)) {
            preRequisites = btAdapter != null && btAdapter.enable();
        }

        if (!preRequisites) {
            showDialog(BLUETOOTH_DISABLED);
            btStatusTextView.setText(getString(R.string.status_bluetooth_disabled));
        } else {
            btStatusTextView.setText(getString(R.string.status_bluetooth_ok));
        }
    }

    private void updateConfig() {
        startActivity(new Intent(this, ConfigActivity.class));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, START_LIVE_DATA, 0, getString(R.string.menu_start_live_data));
        menu.add(0, START_STAND_DATA, 0, getString(R.string.menu_start_stand_data));
        menu.add(0, STOP_LIVE_DATA, 0, getString(R.string.menu_stop_live_data));
        menu.add(0, GET_DTC, 0, getString(R.string.menu_get_dtc));
        menu.add(0, TRIPS_LIST, 0, getString(R.string.menu_trip_list));
        menu.add(0, SETTINGS, 0, getString(R.string.menu_settings));
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case START_LIVE_DATA:
                startLiveData();
                return true;
            case START_STAND_DATA:
                startStandData();
                return true;
            case STOP_LIVE_DATA:
                stopLiveData();
                return true;
            case SETTINGS:
                updateConfig();
                return true;
            case GET_DTC:
                getTroubleCodes();
                return true;
            case TRIPS_LIST:
                startActivity(new Intent(this, TripListActivity.class));
                return true;
        }
        return false;
    }

    private void getTroubleCodes() {
        startActivity(new Intent(this, TroubleCodesActivity.class));
    }

    private void startLiveData() {
        mqueuecounter = 0;
        Log.d(TAG, "Starting live data..");

        tl.removeAllViews(); //start fresh
        doBindService();

        str_driver_name = prefs.getString(ConfigActivity.DRIVER_NAME_KEY,"driver_name");
        //str_driver_name = "JeevanMathew";
        currentTrip = triplog.startTrip();
        fm = new FluctuationMonitor();
        Log.d(TAG, "starting fluxmon  " );
        fm.start();
        if (currentTrip == null)
            showDialog(SAVE_TRIP_NOT_AVAILABLE);

        // start command execution
        new Handler().post(mQueueCommands);
        // screen won't turn off until wakeLock.release()
        wakeLock.acquire();

        /*if (prefs.getBoolean(ConfigActivity.ENABLE_FULL_LOGGING_KEY, false)) {
            // Create the CSV Logger
            long mils = System.currentTimeMillis();
            SimpleDateFormat sdf = new SimpleDateFormat("_dd_MM_yyyy_HH_mm_ss");
            try {
                myCSVWriter = new LogCSVWriter("Log" + sdf.format(new Date(mils)).toString() + ".csv",
                        prefs.getString(ConfigActivity.DIRECTORY_FULL_LOGGING_KEY,
                                getString(R.string.default_dirname_full_logging))
                );
            } catch (FileNotFoundException | RuntimeException e) {
                Log.e(TAG, "Can't enable logging to file.", e);
            }
        }*/
    }


    private void startStandData() {
        Log.d(TAG, "Starting live data..");

        tl.removeAllViews(); //start fresh
        doBindService();

        str_driver_name = prefs.getString(ConfigActivity.DRIVER_NAME_KEY,"driver_name");
        // start command execution
        new Handler().post(mQueueCommands2);
        // screen won't turn off until wakeLock.release()
        wakeLock.acquire();

        /*if (prefs.getBoolean(ConfigActivity.ENABLE_FULL_LOGGING_KEY, false)) {
            // Create the CSV Logger
            long mils = System.currentTimeMillis();
            SimpleDateFormat sdf = new SimpleDateFormat("_dd_MM_yyyy_HH_mm_ss");

            try {
                myCSVWriter = new LogCSVWriter("Log" + sdf.format(new Date(mils)).toString() + ".csv",
                        prefs.getString(ConfigActivity.DIRECTORY_FULL_LOGGING_KEY,
                                getString(R.string.default_dirname_full_logging))
                );
            } catch (FileNotFoundException | RuntimeException e) {
                Log.e(TAG, "Can't enable logging to file.", e);
            }
        }*/
    }

    private void stopLiveData() {
        mqueuecounter2 = 0;
        mqueuecounter = 1;

        Log.d(TAG, "Stopping live data..");
        doUnbindService();
        endTrip();
        releaseWakeLockIfHeld();

        final String devemail = prefs.getString(ConfigActivity.DEV_EMAIL_KEY, "");
        if (devemail != null && !devemail.isEmpty()) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            ObdGatewayService.saveLogcatToFile(getApplicationContext(), devemail);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Where there issues?\nThen please send us the logs.\nSend Logs?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }

        /*if (myCSVWriter != null) {
            myCSVWriter.closeLogCSVWriter();
        }
*/
        if(fm != null && fm.isRunning())
        {
            Log.d(TAG, "stop fluxmon  " );
            fm.stop();
            fm = null;
        } else {
            Log.d(TAG, "fluxmon is not running  " );
            if (fm != null)
            {
                Log.d(TAG, "fluxmon set to null  " );
                fm = null;
            }
        }
    }

    protected void endTrip() {
        if (currentTrip != null) {
            currentTrip.setEndDate(new Date());
            triplog.updateRecord(currentTrip);
        }
    }

    protected Dialog onCreateDialog(int id) {
        AlertDialog.Builder build = new AlertDialog.Builder(this);
        switch (id) {
            case NO_BLUETOOTH_ID:
                build.setMessage(getString(R.string.text_no_bluetooth_id));
                return build.create();
            case BLUETOOTH_DISABLED:
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                return build.create();
            case NO_ORIENTATION_SENSOR:
                build.setMessage(getString(R.string.text_no_orientation_sensor));
                return build.create();
            case NO_GPS_SUPPORT:
                build.setMessage(getString(R.string.text_no_gps_support));
                return build.create();
            case SAVE_TRIP_NOT_AVAILABLE:
                build.setMessage(getString(R.string.text_save_trip_not_available));
                return build.create();
        }
        return null;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem startItem = menu.findItem(START_LIVE_DATA);
        MenuItem stopItem = menu.findItem(STOP_LIVE_DATA);
        MenuItem settingsItem = menu.findItem(SETTINGS);
        MenuItem getDTCItem = menu.findItem(GET_DTC);

        if (service != null && service.isRunning()) {
            getDTCItem.setEnabled(false);
            startItem.setEnabled(false);
            stopItem.setEnabled(true);
            settingsItem.setEnabled(false);
        } else {
            getDTCItem.setEnabled(true);
            stopItem.setEnabled(false);
            startItem.setEnabled(true);
            settingsItem.setEnabled(true);
        }

        return true;
    }

    private void addTableRow(String id, String key, String val) {

        TableRow tr = new TableRow(this);
        MarginLayoutParams params = new ViewGroup.MarginLayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.setMargins(TABLE_ROW_MARGIN, TABLE_ROW_MARGIN, TABLE_ROW_MARGIN,
                TABLE_ROW_MARGIN);
        tr.setLayoutParams(params);

        TextView name = new TextView(this);
        name.setGravity(Gravity.RIGHT);
        name.setText(key + ": ");
        TextView value = new TextView(this);
        value.setGravity(Gravity.LEFT);
        value.setText(val);
        value.setTag(id);
        tr.addView(name);
        tr.addView(value);
        tl.addView(tr, params);
    }

    /**
     *
     */
    private void queueCommands() {
        if (isServiceBound) {
            for (ObdCommand Command : ObdConfig.getCommands()) {
                if (prefs.getBoolean(Command.getName(), true)){
                    service.queueJob(new ObdCommandJob(Command));
                    Log.d(TAG, "in queueCommands  --> " );
            }
            }
        }
    }
    private void queueCommands2() {
        if (isServiceBound) {
            for (ObdCommand Command : ObdConfig2.getCommands()) {
                if (prefs.getBoolean(Command.getName(), true)){
                    service.queueJob2(new ObdCommandJob(Command));
                    Log.d(TAG, "in queueCommands2  --> " );
            }
            }
        }
    }
    private void doBindService() {
        if (!isServiceBound) {
            Log.d(TAG, "Binding OBD service..");
            if (preRequisites) {
                btStatusTextView.setText(getString(R.string.status_bluetooth_connecting));
                Intent serviceIntent = new Intent(this, ObdGatewayService.class);
                bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);
            } else {
                btStatusTextView.setText(getString(R.string.status_bluetooth_disabled));
                Intent serviceIntent = new Intent(this, MockObdGatewayService.class);
                bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);
            }
        }
    }

    private void doUnbindService() {
        Log.d(TAG, "Beginning of doUnbindService()");
        //service.stopService();
        if (isServiceBound) {
            Log.d(TAG, "isServiceBound true, middle doUnbindService()");
            if (/*service != null ||*/service.isRunning()) {
                Log.d(TAG, "service not null or is running  true, stoppService doUnbindService()");
                service.stopService();
                //service = null;
                if (preRequisites)
                    btStatusTextView.setText(getString(R.string.status_bluetooth_ok));
            }
            Log.d(TAG, "Unbinding OBD service..");
            unbindService(serviceConn);
            isServiceBound = false;
            obdStatusTextView.setText(getString(R.string.status_obd_disconnected));
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        setFusedLatitude(location.getLatitude());
        setFusedLongitude(location.getLongitude());
        gpsStatusTextView.setText(getString(R.string.latitude_string) +" "+ getFusedLatitude() + " "+getString(R.string.longitude_string) +" "+ getFusedLongitude());
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onProviderDisabled(String provider) {
    }

    public double getFusedLongitude() {
        return fusedLongitude;
    }

    public double getFusedLatitude() {
        return fusedLatitude;
    }


    public void setFusedLongitude(double lon) {
        fusedLongitude = lon;
    }
    public void setFusedLatitude(double lat) {

        fusedLatitude = lat;
    }

    public void onGpsStatusChanged(int event) {

        switch (event) {
            case GpsStatus.GPS_EVENT_STARTED:
                gpsStatusTextView.setText(getString(R.string.status_gps_started));
                break;
            case GpsStatus.GPS_EVENT_STOPPED:
                gpsStatusTextView.setText(getString(R.string.status_gps_stopped));
                break;
            case GpsStatus.GPS_EVENT_FIRST_FIX:
                gpsStatusTextView.setText(getString(R.string.status_gps_fix));
                break;
            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                btStatusTextView.setText(getString(R.string.status_bluetooth_connected));
            } else {
                Toast.makeText(this, R.string.text_bluetooth_disabled, Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Uploading asynchronous task
     */
    private class UploadAsyncTask extends AsyncTask<ObdReading, Void, Void> {

        @Override
        protected Void doInBackground(ObdReading... readings) {
            Log.d(TAG, "Uploading " + readings.length + " readings..");
            // instantiate reading service client
            final String endpoint = prefs.getString(ConfigActivity.UPLOAD_URL_KEY, "");
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(endpoint)
                    .build();
            ObdService service = restAdapter.create(ObdService.class);
            // upload readings
            for (ObdReading reading : readings) {
                try {
                    Response response = service.uploadReading(reading);
                    assert response.getStatus() == 200;
                } catch (RetrofitError re) {
                    Log.e(TAG, re.toString());
                }

            }
            Log.d(TAG, "Done");
            return null;
        }

    }


    private Integer speed = -1;
    private Integer speedMax = 0;
    private String engineRuntime;
    private static TripRecord2 sInstance;
    private long tripStartTime;
    private float idlingDuration;
    private float drivingDuration;
    private long mAddSpeed;
    private long mSpeedCount;
    private float mDistanceTravel;
    private int mRapidAccTimes;
    private int mRapidDeclTimes;
    private float mInsFuelConsumption = 0.0f;
    private float mDrivingFuelConsumption = 0.0f;
    private float mIdlingFuelConsumption = 0.0f;
    private String mFuelLevel;
    private long mLastTimeStamp;
    private float mFuelTypeValue = 14.7f; // default is Gasoline fuel ratio
    private float mDrivingMaf;
    private int mDrivingMafCount;
    private float mIdleMaf;
    private int mIdleMafCount;
    private int mSecondAgoSpeed;
    private boolean mIsMAFSupported = true;
    private boolean mIsEngineRuntimeSupported = true;
    private boolean mIsTempPressureSupported = true;
    private float mIntakeAirTemp = 0.0f;
    private float mIntakePressure = 0.0f;
    private UUID mTripIdentifier = null;
    private static final int SPEED_GAP = 20;

    private void myTripRecord() {
        tripStartTime = System.currentTimeMillis();
        mTripIdentifier = UUID.randomUUID();
    }

    public void SpeedBasedRuntimeParamCalcs(Integer currentSpeed) {
        calculateIdlingAndDrivingTime(currentSpeed);
        findRapidAccAndDeclTimes(currentSpeed);
        speed = currentSpeed;
        if (speedMax < currentSpeed)
            speedMax = currentSpeed;

        // find travelled distance
        if (speed != 0) {
            mAddSpeed += speed;
            mSpeedCount++;

            mDistanceTravel = (mAddSpeed / mSpeedCount * (drivingDuration / (60 * 60 * 1000)));
        }

    }

    private void findRapidAccAndDeclTimes(Integer currentSpeed) {
        if (speed == -1)
            return;

        if (System.currentTimeMillis() - mLastTimeStamp > 1000) {

            int speedDiff = currentSpeed - mSecondAgoSpeed;
            boolean acceleration = speedDiff > 0;

            if (Math.abs(speedDiff) > SPEED_GAP) {

                if (acceleration) {
                    mRapidAccTimes++;
                    CarData.abnormalSpeedUp = true;
                }else {
                    mRapidDeclTimes++;
                    CarData.abnormalSpeedDown = true;
                }
            }
        }
            mSecondAgoSpeed = currentSpeed;
            mLastTimeStamp = System.currentTimeMillis();
    }


    private void calculateIdlingAndDrivingTime(Integer currentSpeed) {
        long currentTime = System.currentTimeMillis();
        if ((speed == -1 || speed == 0) && currentSpeed == 0) {
            idlingDuration = currentTime - tripStartTime - drivingDuration;
        }
        drivingDuration = currentTime - tripStartTime - idlingDuration;
    }



    private class ServerConnection implements Runnable {
        private final String cmdResult,cmdID;

        public ServerConnection(String cmdID, String cmdResult) {
            this.cmdID = cmdID;
            this.cmdResult = cmdResult;
        }

        @Override
        public void run() {
            try {
                if (socket == null) {
                    socket = new Socket(SERVER_IP, SERVERPORT);
                    clientOutput = new DataOutputStream(socket.getOutputStream());
                }
                //https://en.wikipedia.org/wiki/OBD-II_PIDs#Fuel_Type_Coding
                rsiServerData = new RSIServerData();
                String currentSpeed = null;
                String currentRpm = null;

                if (cmdID.equals("ENGINE_RPM")) {
                    String rpm_val = cmdResult.replace("RPM", "");
                    rpm_old = rpm_val;
                    currentRpm = rpm_old;
                    CarData.driveTorques.add(Integer.valueOf(currentRpm));

                }
                if (cmdID.equals("SPEED")) {
                    String speed_val = cmdResult.replace("km/h", "");
                    speed_old = speed_val;
                    currentSpeed = speed_old;
                    SpeedBasedRuntimeParamCalcs(Integer.valueOf(currentSpeed));
                    CarData.fluctuations.add(Integer.valueOf(currentSpeed));
                    CarData.driveSpeeds.add(Integer.valueOf(currentSpeed));
                }

                if (currentRpm!=null) {
                    rsiServerData.setRpm(Integer.valueOf(currentRpm));
                } else {
                    rsiServerData.setRpm(Integer.valueOf(rpm_old));
                }

                if (currentSpeed != null) {
                    rsiServerData.setSpeed(Integer.valueOf(currentSpeed));
                } else{
                    rsiServerData.setSpeed(Integer.valueOf(speed_old));
                }


                rsiServerData.setDriverName(str_driver_name);
                //rsiServerData.setTripStartTime(tripStartTime);
                //rsiServerData.setmTripIdentifier(mTripIdentifier);
                rsiServerData.setLocation(new com.jeevan.virtualcockpit.server.Location(getFusedLatitude(),getFusedLongitude()));

                if (cmdID.equals(AvailableCommandNames.AIR_INTAKE_TEMP.toString())) {
                    String AIR_INTAKE_TEMP = cmdResult;
                    if(AIR_INTAKE_TEMP != null && !AIR_INTAKE_TEMP.equals("NODATA")) {
                        String temp = AIR_INTAKE_TEMP.replace("C", "");
                        temp=(temp.replace(",", "."));
                        int airIntakeTemp = Integer.valueOf(temp);
                        rsiServerData.setAirIntakeTemp(airIntakeTemp);
                    }else{
                        Log.i(TAG, "Got NODATA for AIR_INTAKE_TEMP doing nothing.... ");
                    }
                }

                if (cmdID.equals(AvailableCommandNames.AMBIENT_AIR_TEMP.toString())) {
                    String AMBIENT_AIR_TEMP = cmdResult;
                    if(AMBIENT_AIR_TEMP != null && !AMBIENT_AIR_TEMP.equals("NODATA")) {
                        String temp = AMBIENT_AIR_TEMP.replace("C", "");
                        temp=(temp.replace(",", "."));
                        int ambientAirTemp = Integer.valueOf(temp);
                        rsiServerData.setAmbientAirTemp(ambientAirTemp);
                    }else {
                        Log.i(TAG, "Got NODATA for AMBIENT_AIR_TEMP doing nothing.... ");
                    }
                }

                if (cmdID.equals(AvailableCommandNames.ENGINE_COOLANT_TEMP.toString())) {
                    String ENGINE_COOLANT_TEMP = cmdResult;
                    if(ENGINE_COOLANT_TEMP != null && !ENGINE_COOLANT_TEMP.equals("NODATA")) {
                        String temp = ENGINE_COOLANT_TEMP.replace("C", "");
                        temp=(temp.replace(",", "."));
                        int engineCoolantTemp = Integer.valueOf(temp);
                        rsiServerData.setEngineCoolantTemp(engineCoolantTemp);
                    }else {
                        Log.i(TAG, "Got NODATA for ENGINE_COOLANT_TEMP doing nothing.... ");
                    }
                }

                if (cmdID.equals(AvailableCommandNames.BAROMETRIC_PRESSURE.toString())) {
                    String BAROMETRIC_PRESSURE = cmdResult;
                    if(BAROMETRIC_PRESSURE != null && !BAROMETRIC_PRESSURE.equals("NODATA")) {
                        String temp = BAROMETRIC_PRESSURE.replace("kPa", "");
                        int barometricPressure = Integer.valueOf(temp);
                        rsiServerData.setBarometricPressure(barometricPressure);
                    } else {
                        Log.i(TAG, "Got NODATA for BAROMETRIC_PRESSURE doing nothing.... ");
                    }
                }

                if (cmdID.equals(AvailableCommandNames.FUEL_PRESSURE.toString())) {
                    String FUEL_PRESSURE = cmdResult;
                    if(FUEL_PRESSURE != null && !FUEL_PRESSURE.equals("NODATA")) {
                        String temp = FUEL_PRESSURE.replace("kPa", "");
                        int fuelPressure = Integer.valueOf(temp);
                        rsiServerData.setFuelPressure(fuelPressure);
                    } else {
                        Log.i(TAG, "Got NODATA for FUEL_PRESSURE doing nothing.... ");
                    }
                }

                if (cmdID.equals(AvailableCommandNames.INTAKE_MANIFOLD_PRESSURE.toString())) {
                    String INTAKE_MANIFOLD_PRESSURE = cmdResult;
                    if(INTAKE_MANIFOLD_PRESSURE != null && !INTAKE_MANIFOLD_PRESSURE.equals("NODATA")) {
                        String temp = INTAKE_MANIFOLD_PRESSURE.replace("kPa", "");
                        int intakeManifoldPressure = Integer.valueOf(temp);
                        rsiServerData.setIntakeManifoldPressure(intakeManifoldPressure);
                    } else {
                        Log.i(TAG, "Got NODATA for INTAKE_MANIFOLD_PRESSURE doing nothing.... ");
                    }
                }

                if (cmdID.equals(AvailableCommandNames.ENGINE_LOAD.toString())) {
                    String ENGINE_LOAD = cmdResult;
                    if(ENGINE_LOAD != null && !ENGINE_LOAD.equals("NODATA")){
                        String temp = ENGINE_LOAD.replace("%", "");
                        double engineLoad = Double.valueOf(temp.replace(",", "."));
                        rsiServerData.setEngineLoad(engineLoad);
                    }else{
                        Log.i(TAG, "Got NODATA for ENGINE_LOAD doing nothing.... ");
                    }
                }

                if (cmdID.equals(AvailableCommandNames.ENGINE_RUNTIME.toString())) {
                    String ENGINE_RUNTIME = cmdResult;
                    if(ENGINE_RUNTIME != null && !ENGINE_RUNTIME.equals("NODATA")) {
                        //Todo...........think
                        rsiServerData.setENGINE_RUNTIME(ENGINE_RUNTIME);
                    } else {
                        Log.i(TAG, "Got NODATA for ENGINE_RUNTIME doing nothing.... ");
                    }
                }

                if (cmdID.equals(AvailableCommandNames.MAF.toString())) {
                    String MAF = cmdResult;
                    if(MAF != null && !MAF.equals("NODATA")) {
                        String temp = MAF.replace("g/s", "");
                        double massAirFlow = Double.valueOf(temp.replace(",", "."));
                        rsiServerData.setMassAirFlow(massAirFlow);
                    } else {
                        Log.i(TAG, "Got NODATA for MAF doing nothing.... ");
                    }
                }

                if (cmdID.equals(AvailableCommandNames.THROTTLE_POS.toString())) {
                    String THROTTLE_POS = cmdResult;
                    if(THROTTLE_POS != null && !THROTTLE_POS.equals("NODATA")) {
                        String temp = THROTTLE_POS.replace("%", "");
                        double throttlePos = Double.valueOf(temp.replace(",", "."));
                        rsiServerData.setThrottlePos(throttlePos);
                    } else {
                        Log.i(TAG, "Got NODATA for THROTTLE_POS doing nothing.... ");
                    }
                }

                if (cmdID.equals(AvailableCommandNames.TROUBLE_CODES.toString())) {
                    String TROUBLE_CODES = cmdResult;
                    if(TROUBLE_CODES != null && !TROUBLE_CODES.equals("NODATA")) {
                        //Todo...........think
                        rsiServerData.setTROUBLE_CODES(TROUBLE_CODES);
                    } else {
                        Log.i(TAG, "Got NODATA for TROUBLE_CODES doing nothing.... ");
                    }
                }

                if (cmdID.equals(AvailableCommandNames.FUEL_LEVEL.toString())) {
                    String FUEL_LEVEL = cmdResult;
                    if(FUEL_LEVEL != null && !FUEL_LEVEL.equals("NODATA")) {
                        String temp = FUEL_LEVEL.replace("%", "");
                        double fuelLevel = Double.valueOf(temp.replace(",", "."));
                        rsiServerData.setFuelLevel(fuelLevel);
                    } else {
                        Log.i(TAG, "Got NODATA for FUEL_LEVEL doing nothing.... ");
                    }
                }

                if (cmdID.equals(AvailableCommandNames.FUEL_TYPE.toString())) {
                    String FUEL_TYPE = cmdResult;
                    if(FUEL_TYPE != null && !FUEL_TYPE.equals("NODATA")) {
                        //Todo...........think
                        rsiServerData.setFUEL_TYPE(FUEL_TYPE);
                    } else {
                        Log.i(TAG, "Got NODATA for FUEL_TYPE doing nothing.... ");
                    }
                }

                if (cmdID.equals(AvailableCommandNames.FUEL_CONSUMPTION_RATE.toString())) {
                        String FUEL_CONSUMPTION_RATE = cmdResult;
                        if(FUEL_CONSUMPTION_RATE != null && !FUEL_CONSUMPTION_RATE.equals("NODATA")) {
                            String temp = FUEL_CONSUMPTION_RATE.replace("L/h", "");
                            float fuelConsumptionRate = Float.valueOf(temp.replace(",", "."));
                            rsiServerData.setFuelLevel(fuelConsumptionRate);
                        }else{
                            Log.i(TAG, "Got NODATA for FUEL_CONSUMPTION_RATE doing nothing.... ");
                        }
                }

                if (cmdID.equals(AvailableCommandNames.TIMING_ADVANCE.toString())) {
                        String TIMING_ADVANCE = cmdResult;
                        if(TIMING_ADVANCE != null && !TIMING_ADVANCE.equals("NODATA")) {
                            String temp = TIMING_ADVANCE.replace("%","");
                            double timingAdvance = Double.valueOf(temp.replace(",","."));
                            rsiServerData.setTimingAdvance(timingAdvance);
                        } else {

                        }
                }

                if (cmdID.equals(AvailableCommandNames.DTC_NUMBER.toString())) {
                        String DTC_NUMBER = cmdResult;
                        if(DTC_NUMBER != null && !DTC_NUMBER.equals("NODATA")) {
                            //Todo https://www.outilsobdfacile.com/data-trouble-code-obd2.php
                            rsiServerData.setDTC_NUMBER(DTC_NUMBER);
                        } else {
                            Log.i(TAG, "Got NODATA for DTC_NUMBER doing nothing.... ");
                        }
                }

                if (cmdID.equals(AvailableCommandNames.EQUIV_RATIO.toString())) {
                        String EQUIV_RATIO = cmdResult;
                        if(EQUIV_RATIO != null && !EQUIV_RATIO.equals("NODATA")) {
                            String temp = EQUIV_RATIO.replace("%", "");
                            double equivRatio = Double.valueOf(temp.replace(",", "."));
                            rsiServerData.setEquivRatio(equivRatio);
                        } else {
                            Log.i(TAG, "Got NODATA for EQUIV_RATIO doing nothing.... ");
                        }
                }

                if (cmdID.equals(AvailableCommandNames.DISTANCE_TRAVELED_AFTER_CODES_CLEARED.toString())) {
                        String DISTANCE_TRAVELED_AFTER_CODES_CLEARED = cmdResult;
                        if(DISTANCE_TRAVELED_AFTER_CODES_CLEARED != null && !DISTANCE_TRAVELED_AFTER_CODES_CLEARED.equals("NODATA")) {
                            String temp =  DISTANCE_TRAVELED_AFTER_CODES_CLEARED.replace("km","");
                            int distanceTraveledAfterCodesCleared = Integer.valueOf(temp);
                            rsiServerData.setDistanceTraveledAfterCodesCleared(distanceTraveledAfterCodesCleared);
                        } else {
                            Log.i(TAG, "Got NODATA for DISTANCE_TRAVELED_AFTER_CODES_CLEARED doing nothing.... ");
                        }
                }

                if (cmdID.equals(AvailableCommandNames.CONTROL_MODULE_VOLTAGE.toString())) {
                        String CONTROL_MODULE_VOLTAGE = cmdResult;
                        if(CONTROL_MODULE_VOLTAGE != null && !CONTROL_MODULE_VOLTAGE.equals("NODATA")) {
                            String temp =  CONTROL_MODULE_VOLTAGE.replace("V","");
                            double controlModuleVoltage = Double.valueOf(temp.replace(",","."));
                            rsiServerData.setControlModuleVoltage(controlModuleVoltage);
                        } else {
                            Log.i(TAG, "Got NODATA for CONTROL_MODULE_VOLTAGE doing nothing.... ");
                        }
                }

                if (cmdID.equals(AvailableCommandNames.ENGINE_FUEL_RATE.toString())) {
                        String ENGINE_FUEL_RATE = cmdResult;
                        if(ENGINE_FUEL_RATE != null && !ENGINE_FUEL_RATE.equals("NODATA")) {
                            String temp = ENGINE_FUEL_RATE.replace("L/s","");
                            double engineFuelRate = Double.valueOf(temp.replace(",","."));
                            rsiServerData.setEngineFuelRate(engineFuelRate);
                        } else {
                            Log.i(TAG, "Got NODATA for ENGINE_FUEL_RATE doing nothing.... ");
                        }
                }

                if (cmdID.equals(AvailableCommandNames.FUEL_RAIL_PRESSURE.toString())) {
                        String FUEL_RAIL_PRESSURE = cmdResult;
                        if(FUEL_RAIL_PRESSURE != null && !FUEL_RAIL_PRESSURE.equals("NODATA")) {
                            String temp = FUEL_RAIL_PRESSURE.replace("kPa","");
                            int fuelRailPressure = Integer.valueOf(temp);
                            rsiServerData.setFuelRailPressure(fuelRailPressure);
                        } else {
                            Log.i(TAG, "Got NODATA for FUEL_RAIL_PRESSURE doing nothing.... ");
                        }
                }
                if (cmdID.equals(AvailableCommandNames.VIN.toString())) {
                        String VIN = cmdResult;
                        if(VIN != null && !VIN.equals("NODATA")) {
                            rsiServerData.setVIN(VIN);
                        } else {
                            Log.i(TAG, "Got NODATA for VIN doing nothing.... ");
                        }
                }

                if (cmdID.equals(AvailableCommandNames.DISTANCE_TRAVELED_MIL_ON.toString())) {
                        String DISTANCE_TRAVELED_MIL_ON = cmdResult;
                        if(DISTANCE_TRAVELED_MIL_ON != null && !DISTANCE_TRAVELED_MIL_ON.equals("NODATA")) {
                            String temp = DISTANCE_TRAVELED_MIL_ON.replace("km", "");
                            int distanceTraveledMilOn = Integer.valueOf(temp);
                            rsiServerData.setDistanceTraveledMilOn(distanceTraveledMilOn);
                        }else{
                            Log.i(TAG, "Got NODATA for ENGINE_OIL_TEMP doing nothing.... ");
                        }
                }

                //TODO research -> Cant find following both in orig app as stand data so commenting out:
                if (cmdID.equals(AvailableCommandNames.TIME_TRAVELED_MIL_ON.toString())) {
                        String TIME_TRAVELED_MIL_ON = cmdResult;
                        if(TIME_TRAVELED_MIL_ON != null && !TIME_TRAVELED_MIL_ON.equals("NODATA")) {
                            rsiServerData.setTIME_TRAVELED_MIL_ON(TIME_TRAVELED_MIL_ON);
                        } else {
                            Log.i(TAG, "Got NODATA for TIME_TRAVELED_MIL_ON doing nothing.... ");
                        }
                }

                if (cmdID.equals(AvailableCommandNames.TIME_SINCE_TC_CLEARED.toString())) {
                        String TIME_SINCE_TC_CLEARED = cmdResult;
                        if(TIME_SINCE_TC_CLEARED != null && !TIME_SINCE_TC_CLEARED.equals("NODATA")) {
                            rsiServerData.setTIME_SINCE_TC_CLEARED(TIME_SINCE_TC_CLEARED);
                        } else {
                            Log.i(TAG, "Got NODATA for TIME_SINCE_TC_CLEARED doing nothing.... ");
                        }
                }

                if (cmdID.equals(AvailableCommandNames.REL_THROTTLE_POS.toString())) {
                        String REL_THROTTLE_POS = cmdResult;
                        if(REL_THROTTLE_POS != null && !REL_THROTTLE_POS.equals("NODATA")) {
                            String temp = REL_THROTTLE_POS.replace("%", "");
                            double relThrottlePos = Double.valueOf(temp.replace(",", "."));
                            rsiServerData.setRelThrottlePos(relThrottlePos);
                        } else {
                            Log.i(TAG, "Got NODATA for REL_THROTTLE_POS doing nothing.... ");
                        }
                }
                /*
                PIDS_01_20("Available PIDs 01-20"),
                PIDS_21_40("Available PIDs 21-40"),
                PIDS_41_60("Available PIDs 41-60"),
                 */
                //TODO research -> Cant find following both in orig app as stand data so commenting out:
                /*if (cmdID.equals(AvailableCommandNames.PIDS.toString())) {
                        String PIDS = cmdResult;
                        rsiServerData.setPIDS(PIDS);
                }*/

                if (cmdID.equals(AvailableCommandNames.ABS_LOAD.toString())) {
                        String ABS_LOAD = cmdResult;
                        if(ABS_LOAD != null && !ABS_LOAD.equals("NODATA")) {
                            String temp = ABS_LOAD.replace("%","");
                            //temp = temp.replace("-","");
                            double absLoad = Double.valueOf(temp.replace(",","."));
                            rsiServerData.setAbsLoad(absLoad);
                        } else {
                            Log.i(TAG, "Got NODATA for ABS_LOAD doing nothing.... ");
                        }
                }
                if (cmdID.equals(AvailableCommandNames.ENGINE_OIL_TEMP.toString())) {
                        String ENGINE_OIL_TEMP = cmdResult;
                        if(ENGINE_OIL_TEMP != null && !ENGINE_OIL_TEMP.equals("NODATA")) {
                            String temp = ENGINE_OIL_TEMP.replace("C", "");
                            temp=(temp.replace(",", "."));
                            float engineOilTemp = Float.valueOf(temp);
                            rsiServerData.setEngineOilTemp(engineOilTemp);
                        }else{
                            Log.i(TAG, "Got NODATA for ENGINE_OIL_TEMP doing nothing.... ");

                        }
                }

                if (cmdID.equals(AvailableCommandNames.AIR_FUEL_RATIO.toString())) {
                        String AIR_FUEL_RATIO = cmdResult;
                        if(AIR_FUEL_RATIO != null && !AIR_FUEL_RATIO.equals("NODATA")) {
                            String temp = AIR_FUEL_RATIO.replace(":1 AFR", "");
                            double airFuelRatio = Double.valueOf(temp.replace(",","."));
                            rsiServerData.setAirFuelRatio(airFuelRatio);
                        } else {
                            Log.i(TAG, "Got NODATA for AIR_FUEL_RATIO doing nothing.... ");
                        }
                }

                if (cmdID.equals(AvailableCommandNames.WIDEBAND_AIR_FUEL_RATIO.toString())) {
                        String WIDEBAND_AIR_FUEL_RATIO = cmdResult;
                        if(WIDEBAND_AIR_FUEL_RATIO != null && !WIDEBAND_AIR_FUEL_RATIO.equals("NODATA")) {
                            String temp = WIDEBAND_AIR_FUEL_RATIO.replace(":1 AFR", "");
                            double widebandAirFuelRatio = Double.valueOf(temp.replace(",","."));
                            rsiServerData.setWidebandAirFuelRatio(widebandAirFuelRatio);
                        } else {
                            Log.i(TAG, "Got NODATA for WIDEBAND_AIR_FUEL_RATIO doing nothing.... ");
                        }
                }

                //TODO research -> Cant find following  in orig app as stand data so commenting out:
                /*if (cmdID.equals(AvailableCommandNames.DTC_NUMBER.toString())) {
                        String DTC_NUMBER = cmdResult;
                        rsiServerData.setDTC_NUMBER(DTC_NUMBER);
                }*/
                    //Log.d(TAG, "------------CarData.abnormalSpeedDown  ----------> " + CarData.abnormalSpeedDown);
                if (CarData.abnormalSpeedDown) {
                        rsiServerData.setAbnormalSpeedDown(true);
                }
                //Log.d(TAG, "------------CarData.abnormalSpeedUp  ----------> " + CarData.abnormalSpeedUp);
                if (CarData.abnormalSpeedUp){
                    rsiServerData.setAbnormalSpeedUp(true);
                }


                //rsiServerData.setDistanceTravelledInLast(CarData.DISTANCE_TRAVELLED);
                if(speedMax != 0 )
                {
                    rsiServerData.setSpeedMax(speedMax);
                }

                if (drivingDuration != 0){
                    rsiServerData.setDrivingDuration(drivingDuration);
                }

                if(mRapidAccTimes != 0){
                    rsiServerData.setmRapidAccTimes(mRapidAccTimes);
                }

                if (mRapidDeclTimes != 0){
                    rsiServerData.setmRapidDeclTimes(mRapidDeclTimes);
                }

                if (idlingDuration > 0){
                    rsiServerData.setIdlingDuration(idlingDuration);
                }

                if(mDistanceTravel !=0 ) {
                    rsiServerData.setmDistanceTravel(mDistanceTravel);
                }

                if(CarData.driveScore != 0) {
                    rsiServerData.setDrivingScore(CarData.driveScore);
                }

                clientOutput.write(JSONData.createRsiOutput(rsiServerData).getBytes());
                clientOutput.flush();
                //socket.close();

                } catch(IOException e){
                    e.printStackTrace();
                }
        }
    }
}