/*

 * COPY RIGHT INFORMATION
 * ---------------------------
 * Authors:
 *
 * Mr.Jeevan Mathew ,
 * Kulathapappallil House ,
 * Kadakkad ,
 * Pandalam PO 689501, Kerala ,
 * Email ID : jmathew@gmx.de
 * aka "JM"

 * and

 * Tarek El-Sibay,
 * Am Golfplatz 15,
 * 24576 Bad Bramstedt,
 * Deutschland
 * aka "TES"

 * retain all ownership and intellectual property rights in the code accompanying this message and in all derivatives hereto.
 * Previously not existent features/mechanisms have been added, where the authors JM and TES definitively claim their rights
 * that thirds need to obtain there written consent for further use.
 * Once written consent is obtained this software and system is licensed under Apache License Version 2.0
 *
 * REVISION HISTORY
 * ---------------------------
 * @author   :
 * @since    :
 * @comments :
 *
 * ---------------------------
 *
 * DESCRIPTION
 * ---------------------------
 * This software has been originally taken and from

 * https://github.com/pires/android-obd-reader
 * https://github.com/pires/obd-java-api
 *
 *
 *
 * ---------------------------
 */

package com.jeevan.virtualcockpit.server;

import java.util.UUID;

//https://en.wikipedia.org/wiki/OBD-II_PIDs#Fuel_Type_Coding

public class RSIServerData {

    private String apiName = "OBD";
    private UUID mTripIdentifier;
    //private String speed;
    private String fuellevel;
    private  double fuelLevel;
    private String engineruntime;
    //private String rpm;
    //private String engineLoad;
    private double engineLoad;
    private int ect;
    private String engine_Coolant_temperature;
    private String driverName;
    private boolean abnormalSpeedUp;
    private boolean abnormalSpeedDown;
    private int drivingScore;
    private long tripStartTime;
    private float drivingDuration;
    private float mDistanceTravel;
    private int mRapidAccTimes;
    private int mRapidDeclTimes;
    private float idlingDuration;
    private Integer speedMax;
    private int drivescore;
    private String distanceTravelledInLast;
    private Location location;

    private String ENGINE_RPM;//("Engine RPM"),
    private int speed;
    private int rpm;
    private String SPEED;//("Vehicle Speed"),
    private String AIR_INTAKE_TEMP;//("Air Intake Temperature"),
    private int airIntakeTemp;
    private String AMBIENT_AIR_TEMP;//("Ambient Air Temperature"),
    private int ambientAirTemp;
    private String ENGINE_COOLANT_TEMP;//("Engine Coolant Temperature"),
    private int engineCoolantTemp;
    private String BAROMETRIC_PRESSURE;//("Barometric Pressure"),
    private int barometricPressure;
    private String FUEL_PRESSURE;//("Fuel Pressure"),
    private int fuelPressure;
    private String INTAKE_MANIFOLD_PRESSURE;//("Intake Manifold Pressure"),
    private int intakeManifoldPressure;
    private String ENGINE_LOAD;//("Engine Load"),
    private String ENGINE_RUNTIME;//("Engine Runtime"),
    //private String ENGINE_RPM;//("Engine RPM"),
    //private String SPEED;//("Vehicle Speed"),
    private String MAF;//("Mass Air Flow"),
    private double massAirFlow;
    private String THROTTLE_POS;//("Throttle Position"),

    private double throttlePos;

    private String TROUBLE_CODES;//("Trouble Codes"),
    private String FUEL_LEVEL;//("Fuel Level"),
    private String FUEL_TYPE;//("Fuel Type"),
    private String FUEL_CONSUMPTION_RATE;//("Fuel Consumption Rate"),
    private float fuelConsumptionRate;
    private String TIMING_ADVANCE;//("Timing Advance"),
    private double timingAdvance;
    private String DTC_NUMBER;//("Diagnostic Trouble Codes"),
    private String EQUIV_RATIO;//("Command Equivalence Ratio"),
    private double equivRatio;
    private String DISTANCE_TRAVELED_AFTER_CODES_CLEARED;//("Distance since codes cleared"),
    private int distanceTraveledAfterCodesCleared;
    private String CONTROL_MODULE_VOLTAGE;//("Control Module Power Supply "),
    private double controlModuleVoltage;
    private String ENGINE_FUEL_RATE;//("Engine Fuel Rate"),
    private double engineFuelRate;
    private String FUEL_RAIL_PRESSURE;//("Fuel Rail Pressure"),
    private int fuelRailPressure;
    private String VIN;//("Vehicle Identification Number (VIN)"),
    private String DISTANCE_TRAVELED_MIL_ON;//("Distance traveled with MIL on")
    private int distanceTraveledMilOn;// ,
    private String TIME_TRAVELED_MIL_ON;//("Time run with MIL on"),
    private String TIME_SINCE_TC_CLEARED;//("Time since trouble codes cleared"),
    private String REL_THROTTLE_POS;//("Relative throttle position"),
    private double relThrottlePos;
    private String PIDS;//("Available PIDs"),
    private String ABS_LOAD;//("Absolute load"),
    private double absLoad;
    private String ENGINE_OIL_TEMP;//("Engine oil temperature"),
    private float engineOilTemp;
    private String AIR_FUEL_RATIO;//("Air/Fuel Ratio"),
    private double airFuelRatio;
    private String WIDEBAND_AIR_FUEL_RATIO;//("Wideband Air/Fuel Ratio");
    private double widebandAirFuelRatio;

    public RSIServerData() {
    }

    public int getSpeed() {
        return speed;
    }
    public void setSpeed(int speed) {
        this.speed = speed;
    }
    public int getRpm() {
        return rpm;
    }
    public void setRpm(int rpm) {
        this.rpm = rpm;
    }

    public String getApiName() {
        return apiName;
    }
    public void setApiName(String apiName) {
        this.apiName = apiName;
    }
    public String getDriverName() {
        return driverName;
    }
    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public Location getLocation() {
        return location;
    }
    public void setLocation(Location location) {
        this.location = location;
    }

    public int getDrivingScore() {
        return drivingScore;
    }
    public void setDrivingScore(int drivingScore) {
        this.drivingScore = drivingScore;
    }
    public double getmDistanceTravel() {
        return mDistanceTravel;
    }
    public void setmDistanceTravel(float mDistanceTravel) {
        this.mDistanceTravel = mDistanceTravel;
    }
    public int getmRapidAccTimes() {
        return mRapidAccTimes;
    }
    public void setmRapidAccTimes(int mRapidAccTimes) {
        this.mRapidAccTimes = mRapidAccTimes;
    }
    public int getmRapidDeclTimes() {
        return mRapidDeclTimes;
    }
    public void setmRapidDeclTimes(int mRapidDeclTimes) {
        this.mRapidDeclTimes = mRapidDeclTimes;
    }
    public long getTripStartTime() {
        return tripStartTime;
    }
    public void setTripStartTime(long tripStartTime) {
        this.tripStartTime = tripStartTime;
    }
    /*public int getSpeedMax() {
        return speedMax;
    }
    public void setSpeedMax(int speedMax) {
        this.speedMax = speedMax;
    }*/
    public float getDrivescore() {
        return drivescore;
    }
    public void setDrivescore(int drivescore) {
        this.drivescore = drivescore;
    }
    public UUID getmTripIdentifier() {
        return mTripIdentifier;
    }
    public void setmTripIdentifier(UUID mTripIdentifier) {
        this.mTripIdentifier = mTripIdentifier;
    }
    public float getDrivingDuration() {
        return drivingDuration;
    }
    public void setDrivingDuration(float drivingDuration) {
        this.drivingDuration = drivingDuration;
    }
    public String getDistanceTravelledInLast() {
        return distanceTravelledInLast;
    }
    public void setDistanceTravelledInLast(String distanceTravelledInLast) {
        this.distanceTravelledInLast = distanceTravelledInLast;
    }
    public boolean isAbnormalSpeedUp() {
        return abnormalSpeedUp;
    }
    public void setAbnormalSpeedUp(boolean abnormalSpeedUp) {
        this.abnormalSpeedUp = abnormalSpeedUp;
    }
    public boolean isAbnormalSpeedDown() {
        return abnormalSpeedDown;
    }
    public void setAbnormalSpeedDown(boolean abnormalSpeedDown) {
        this.abnormalSpeedDown = abnormalSpeedDown;
    }
    public String getENGINE_RPM() {
        return ENGINE_RPM;
    }
    public void setENGINE_RPM(String ENGINE_RPM) {
        this.ENGINE_RPM = ENGINE_RPM;
    }
    public String getSPEED() {
        return SPEED;
    }
    public void setSPEED(String SPEED) {
        this.SPEED = SPEED;
    }
    public String getAIR_INTAKE_TEMP() {
        return AIR_INTAKE_TEMP;
    }
    public void setAIR_INTAKE_TEMP(String AIR_INTAKE_TEMP) {
        this.AIR_INTAKE_TEMP = AIR_INTAKE_TEMP;
    }
    public String getAMBIENT_AIR_TEMP() {
        return AMBIENT_AIR_TEMP;
    }
    public void setAMBIENT_AIR_TEMP(String AMBIENT_AIR_TEMP) {
        this.AMBIENT_AIR_TEMP = AMBIENT_AIR_TEMP;
    }
    public String getENGINE_COOLANT_TEMP() {
        return ENGINE_COOLANT_TEMP;
    }
    public void setENGINE_COOLANT_TEMP(String ENGINE_COOLANT_TEMP) {
        this.ENGINE_COOLANT_TEMP = ENGINE_COOLANT_TEMP;
    }
    public String getBAROMETRIC_PRESSURE() {
        return BAROMETRIC_PRESSURE;
    }
    public void setBAROMETRIC_PRESSURE(String BAROMETRIC_PRESSURE) {
        this.BAROMETRIC_PRESSURE = BAROMETRIC_PRESSURE;
    }
    public String getFUEL_PRESSURE() {
        return FUEL_PRESSURE;
    }
    public void setFUEL_PRESSURE(String FUEL_PRESSURE) {
        this.FUEL_PRESSURE = FUEL_PRESSURE;
    }
    public String getINTAKE_MANIFOLD_PRESSURE() {
        return INTAKE_MANIFOLD_PRESSURE;
    }
    public void setINTAKE_MANIFOLD_PRESSURE(String INTAKE_MANIFOLD_PRESSURE) {
        this.INTAKE_MANIFOLD_PRESSURE = INTAKE_MANIFOLD_PRESSURE;
    }
    public String getENGINE_LOAD() {
        return ENGINE_LOAD;
    }
    public void setENGINE_LOAD(String ENGINE_LOAD) {
        this.ENGINE_LOAD = ENGINE_LOAD;
    }
    public String getENGINE_RUNTIME() {
        return ENGINE_RUNTIME;
    }
    public void setENGINE_RUNTIME(String ENGINE_RUNTIME) {
        this.ENGINE_RUNTIME = ENGINE_RUNTIME;
    }
    public String getMAF() {
        return MAF;
    }
    public void setMAF(String MAF) {
        this.MAF = MAF;
    }
    public String getTHROTTLE_POS() {
        return THROTTLE_POS;
    }
    public void setTHROTTLE_POS(String THROTTLE_POS) {
        this.THROTTLE_POS = THROTTLE_POS;
    }
    public String getTROUBLE_CODES() {
        return TROUBLE_CODES;
    }
    public void setTROUBLE_CODES(String TROUBLE_CODES) {
        this.TROUBLE_CODES = TROUBLE_CODES;
    }
    public String getFUEL_TYPE() {
        return FUEL_TYPE;
    }
    public void setFUEL_TYPE(String FUEL_TYPE) {
        this.FUEL_TYPE = FUEL_TYPE;
    }
    public String getFUEL_CONSUMPTION_RATE() {
        return FUEL_CONSUMPTION_RATE;
    }
    public void setFUEL_CONSUMPTION_RATE(String FUEL_CONSUMPTION_RATE) {
        this.FUEL_CONSUMPTION_RATE = FUEL_CONSUMPTION_RATE;
    }
    public String getTIMING_ADVANCE() {
        return TIMING_ADVANCE;
    }
    public void setTIMING_ADVANCE(String TIMING_ADVANCE) {
        this.TIMING_ADVANCE = TIMING_ADVANCE;
    }
    public String getDTC_NUMBER() {
        return DTC_NUMBER;
    }
    public void setDTC_NUMBER(String DTC_NUMBER) {
        this.DTC_NUMBER = DTC_NUMBER;
    }
    public String getDISTANCE_TRAVELED_AFTER_CODES_CLEARED() {
        return DISTANCE_TRAVELED_AFTER_CODES_CLEARED;
    }
    public void setDISTANCE_TRAVELED_AFTER_CODES_CLEARED(String DISTANCE_TRAVELED_AFTER_CODES_CLEARED) {
        this.DISTANCE_TRAVELED_AFTER_CODES_CLEARED = DISTANCE_TRAVELED_AFTER_CODES_CLEARED;
    }
    public String getCONTROL_MODULE_VOLTAGE() {
        return CONTROL_MODULE_VOLTAGE;
    }
    public void setCONTROL_MODULE_VOLTAGE(String CONTROL_MODULE_VOLTAGE) {
        this.CONTROL_MODULE_VOLTAGE = CONTROL_MODULE_VOLTAGE;
    }
    public String getENGINE_FUEL_RATE() {
        return ENGINE_FUEL_RATE;
    }
    public void setENGINE_FUEL_RATE(String ENGINE_FUEL_RATE) {
        this.ENGINE_FUEL_RATE = ENGINE_FUEL_RATE;
    }
    public String getFUEL_RAIL_PRESSURE() {
        return FUEL_RAIL_PRESSURE;
    }
    public void setFUEL_RAIL_PRESSURE(String FUEL_RAIL_PRESSURE) {
        this.FUEL_RAIL_PRESSURE = FUEL_RAIL_PRESSURE;
    }
    public String getVIN() {
        return VIN;
    }
    public void setVIN(String VIN) {
        this.VIN = VIN;
    }
    public String getDISTANCE_TRAVELED_MIL_ON() {
        return DISTANCE_TRAVELED_MIL_ON;
    }
    public void setDISTANCE_TRAVELED_MIL_ON(String DISTANCE_TRAVELED_MIL_ON) {
        this.DISTANCE_TRAVELED_MIL_ON = DISTANCE_TRAVELED_MIL_ON;
    }
    public String getTIME_TRAVELED_MIL_ON() {
        return TIME_TRAVELED_MIL_ON;
    }
    public void setTIME_TRAVELED_MIL_ON(String TIME_TRAVELED_MIL_ON) {
        this.TIME_TRAVELED_MIL_ON = TIME_TRAVELED_MIL_ON;
    }
    public String getTIME_SINCE_TC_CLEARED() {
        return TIME_SINCE_TC_CLEARED;
    }
    public void setTIME_SINCE_TC_CLEARED(String TIME_SINCE_TC_CLEARED) {
        this.TIME_SINCE_TC_CLEARED = TIME_SINCE_TC_CLEARED;
    }
    public String getREL_THROTTLE_POS() {
        return REL_THROTTLE_POS;
    }
    public void setREL_THROTTLE_POS(String REL_THROTTLE_POS) {
        this.REL_THROTTLE_POS = REL_THROTTLE_POS;
    }
    public String getPIDS() {
        return PIDS;
    }
    public void setPIDS(String PIDS) {
        this.PIDS = PIDS;
    }
    public String getABS_LOAD() {
        return ABS_LOAD;
    }
    public void setABS_LOAD(String ABS_LOAD) {
        this.ABS_LOAD = ABS_LOAD;
    }
    public String getENGINE_OIL_TEMP() {
        return ENGINE_OIL_TEMP;
    }
    public void setENGINE_OIL_TEMP(String ENGINE_OIL_TEMP) {
        this.ENGINE_OIL_TEMP = ENGINE_OIL_TEMP;
    }
    public String getAIR_FUEL_RATIO() {
        return AIR_FUEL_RATIO;
    }
    public void setAIR_FUEL_RATIO(String AIR_FUEL_RATIO) {
        this.AIR_FUEL_RATIO = AIR_FUEL_RATIO;
    }
    public String getWIDEBAND_AIR_FUEL_RATIO() {
        return WIDEBAND_AIR_FUEL_RATIO;
    }
    public void setWIDEBAND_AIR_FUEL_RATIO(String WIDEBAND_AIR_FUEL_RATIO) {
        this.WIDEBAND_AIR_FUEL_RATIO = WIDEBAND_AIR_FUEL_RATIO;
    }
    public double getFuelLevel() {
        return fuelLevel;
    }
    public void setFuelLevel(double fuelLevel) {
        this.fuelLevel = fuelLevel;
    }
    public double getControlModuleVoltage() {
        return controlModuleVoltage;
    }
    public void setControlModuleVoltage(double controlModuleVoltage) {
        this.controlModuleVoltage = controlModuleVoltage;
    }
    public double getEquivRatio() {
        return equivRatio;
    }
    public void setEquivRatio(double equivRatio) {
        this.equivRatio = equivRatio;
    }
    public double getAmbientAirTemp() {
        return ambientAirTemp;
    }
    public void setAmbientAirTemp(int ambientAirTemp) {
        this.ambientAirTemp = ambientAirTemp;
    }
    public double getTimingAdvance() {
        return timingAdvance;
    }
    public void setTimingAdvance(double timingAdvance) {
        this.timingAdvance = timingAdvance;
    }
    public double getEngineLoad() {
        return engineLoad;
    }
    public void setEngineLoad(double engineLoad) {
        this.engineLoad = engineLoad;
    }
    public double getMassAirFlow() {
        return massAirFlow;
    }
    public void setMassAirFlow(double massAirFlow) {
        this.massAirFlow = massAirFlow;
    }
    public double getThrottlePos() {
        return throttlePos;
    }
    public void setThrottlePos(double throttlePos) {
        this.throttlePos = throttlePos;
    }
    public float getFuelConsumptionRate() {
        return fuelConsumptionRate;
    }
    public void setFuelConsumptionRate(float fuelConsumptionRate) {
        this.fuelConsumptionRate = fuelConsumptionRate;
    }
    public double getDistanceTraveledMilOn() {
        return distanceTraveledMilOn;
    }
    public void setDistanceTraveledMilOn(int distanceTraveledMilOn) {
        this.distanceTraveledMilOn = distanceTraveledMilOn;
    }
    public double getEngineCoolantTemp() {
        return engineCoolantTemp;
    }
    public void setEngineCoolantTemp(int engineCoolantTemp) {
        this.engineCoolantTemp = engineCoolantTemp;
    }
    public double getAirIntakeTemp() {
        return airIntakeTemp;
    }
    public void setAirIntakeTemp(int airIntakeTemp) {
        this.airIntakeTemp = airIntakeTemp;
    }
    public double getBarometricPressure() {
        return barometricPressure;
    }
    public void setBarometricPressure(int barometricPressure) {
        this.barometricPressure = barometricPressure;
    }
    public double getFuelPressure() {
        return fuelPressure;
    }
    public void setFuelPressure(int fuelPressure) {
        this.fuelPressure = fuelPressure;
    }
    public double getIntakeManifoldPressure() {
        return intakeManifoldPressure;
    }
    public void setIntakeManifoldPressure(int intakeManifoldPressure) {
        this.intakeManifoldPressure = intakeManifoldPressure;
    }
    public double getDistanceTraveledAfterCodesCleared() {
        return distanceTraveledAfterCodesCleared;
    }
    public void setDistanceTraveledAfterCodesCleared(int distanceTraveledAfterCodesCleared) {
        this.distanceTraveledAfterCodesCleared = distanceTraveledAfterCodesCleared;
    }
    public float getEngineOilTemp() {
        return engineOilTemp;
    }
    public void setEngineOilTemp(float engineOilTemp) {
        this.engineOilTemp = engineOilTemp;
    }

    public double getEngineFuelRate() {
        return engineFuelRate;
    }
    public void setEngineFuelRate(double engineFuelRate) {
        this.engineFuelRate = engineFuelRate;
    }
    public double getFuelRailPressure() {
        return fuelRailPressure;
    }
    public void setFuelRailPressure(int fuelRailPressure) {
        this.fuelRailPressure = fuelRailPressure;
    }
    public double getRelThrottlePos() {
        return relThrottlePos;
    }
    public void setRelThrottlePos(double relThrottlePos) {
        this.relThrottlePos = relThrottlePos;
    }
    public double getAbsLoad() {
        return absLoad;
    }
    public void setAbsLoad(double absLoad) {
        this.absLoad = absLoad;
    }
    public String getFUEL_LEVEL() {
        return FUEL_LEVEL;
    }
    public void setFUEL_LEVEL(String FUEL_LEVEL) {
        this.FUEL_LEVEL = FUEL_LEVEL;
    }
    public String getEQUIV_RATIO() {
        return EQUIV_RATIO;
    }
    public void setEQUIV_RATIO(String EQUIV_RATIO) {
        this.EQUIV_RATIO = EQUIV_RATIO;
    }
    public String getEngineruntime() {
        return engineruntime;
    }
    public void setEngineruntime(String engineruntime) {
        this.engineruntime = engineruntime;
    }
    public String getFuellevel() {
        return fuellevel;
    }
    public void setFuellevel(String fuellevel) {
        this.fuellevel = fuellevel;
    }

    public float getIdlingDuration() {
        return idlingDuration;
    }

    public void setIdlingDuration(float idlingDuration) {
        this.idlingDuration = idlingDuration;
    }

    public Integer getSpeedMax() {
        return speedMax;
    }

    public void setSpeedMax(Integer speedMax) {
        this.speedMax = speedMax;
    }

    public double getAirFuelRatio() {
        return airFuelRatio;
    }

    public void setAirFuelRatio(double airFuelRatio) {
        this.airFuelRatio = airFuelRatio;
    }

    public double getWidebandAirFuelRatio() {
        return widebandAirFuelRatio;
    }

    public void setWidebandAirFuelRatio(double widebandAirFuelRatio) {
        this.widebandAirFuelRatio = widebandAirFuelRatio;
    }
}